implementation module ShipTypes

import iTasks
import Math.Geometry
import _SystemArray

instance == Kind where (==) x y = x === y
instance == Country where (==) x y = x === y

distance :: a b -> Real | toPosition a & toPosition b
distance a b = r * sqrt (dLat ^ 2.0 + (cos mLat * dLong) ^ 2.0)
where
	p = toPosition a
	q = toPosition b
	r = 6371.009 // radius earth in km
	dLat = (p.lat - q.lat) * pi / 180.0
	mLat = (p.lat + q.lat) * pi / 360.0
	dLong = (p.lon - q.lon) * pi / 180.0
	mLong = (p.lon + q.lon) * pi / 360.0

class toPosition a :: a -> Position
instance toPosition Position where toPosition p = p
instance toPosition Ship     where toPosition s = s.Ship.position
instance toPosition Park where toPosition w = w.Park.position
instance toPosition Port     where toPosition p = p.Port.position
instance toPosition Res where
	toPosition (ShipResult x) = toPosition x
	toPosition (ParkResult x) = toPosition x
	toPosition (PortResult x) = toPosition x
	toPosition _ = abort "No toPosition for this Res"
	
instance == Position where (==) x y = x === y
instance == Ship where (==) x y = x === y
instance == Park where (==) x y = x === y

derive class iTask Ship, Park, Port, Kind, Position, Country, Status //, Res
gDefault{|Res|} = RealResult 42.0
derive gEq Res
derive JSONEncode Res
derive JSONDecode Res
gText{|Res|} mode (?Just res) = case res of
	ShipResult x	= gText{|*|} mode (?Just x)
	ParkResult x	= gText{|*|} mode (?Just x)
	PortResult x	= gText{|*|} mode (?Just x)
	StringResult x	= gText{|*|} mode (?Just x)
	RealResult x	= gText{|*|} mode (?Just x)
	BoolResult x	= gText{|*|} mode (?Just x)
gText{|Res|} mode ?None = [""]
derive gEditor Res

instance == Res where (==) x y = x === y

instance < Res where
	(<) (RealResult x) (RealResult y) = x < y
	(<) (StringResult x) (StringResult y) = x < y
	(<) _ _ = abort "no instance of < for these Res values"

class toRes x :: x -> Res
instance toRes Ship		where toRes x = ShipResult x
instance toRes Bool		where toRes x = BoolResult x
instance toRes Real		where toRes x = RealResult x
instance toRes Park where toRes x = ParkResult x
instance toRes Port		where toRes x = PortResult x
instance toRes String	where toRes x = StringResult x

instance toString Ship		where toString x = hd (gText{|*|} AsSingleLine (?Just x))
instance toString Port		where toString x = hd (gText{|*|} AsSingleLine (?Just x))
instance toString Park	where toString x = hd (gText{|*|} AsSingleLine (?Just x))
instance toString Position	where toString x = hd (gText{|*|} AsSingleLine (?Just x))
instance toString Kind		where toString x = hd (gText{|*|} AsSingleLine (?Just x))
instance toString Country	where toString x = hd (gText{|*|} AsSingleLine (?Just x))
instance toString Status	where toString x = hd (gText{|*|} AsSingleLine (?Just x))

matchName :: String String -> Bool
matchName name patt = match [c \\ c <-: patt] [c \\ c <-: name]
where
	match [] _ = True
	match ['.':r] [c:x] = match r x
	match l=:['*':p:r] [c:x] | toLower p == toLower c
		= match r x
		= match l x
	match [p:r] [c:x] | toLower p == toLower c
		= match r x
		= False
	match _ _ = False

// ====

ships :: [Ship]
ships =
  [	{ name = "TX10 Emmie"
	, size = 41
	, kind = Fisher
	, flag = NL
//	, owner = "Frido Boom"
	, status = Moored
	, position = {lat = 53.03957, lon = 4.85033}
	}
  ,	{ name = "TX27 NOVA CURA"
	, size = 23
	, kind = Fisher
	, flag = NL
//	, owner = "M. Hutjes"
	, status = Sailing
	, position = {lat = 53.04125, lon = 4.85224}
	}
  ,	{ name = "HNLMS Tromp"
	, size = 144
	, kind = Navy
	, flag = NL
//	, owner = "King Willem"
	, status = Moored
	, position = {lat = 52.95939, lon = 4.79615}
	}
  ,	{ name = "NATO WARShipResult M111"
	, size = 53
	, kind = Navy
	, flag = UK
//	, owner = "Queen Elizabeth"
	, status = Sailing
	, position = {lat = 52.98099, lon = 4.76725}
	}
  ,	{ name = "HNLMS Schiedam"
	, size = 52
	, kind = Navy
	, flag = NL
//	, owner = "King Willem"
	, status = Sailing
	, position = {lat = 52.98308, lon = 4.57119}
	}
  ,	{ name = "MSC Bari"
	, size = 366
	, kind = Cargo
	, flag = LR
//	, owner = "Maersk"
	, status = Sailing
	, position = {lat = 52.04863, lon = 3.56977}
	}
  ,	{ name = "Sea empress" 
	, size = 182
	, kind = Tanker
	, flag = MT
//	, owner = "Thenamaris"
	, status = Sailing
	, position = {lat = 51.90038, lon = 3.5334}
	}
/*
  ,	{ name =
	, size =
	, kind =
	, flag =
	, owner =
	, status = 
	, position = {lat = , lon = }
	}
*/
  ]

windParks :: [Park]
windParks = 
  [	{ name = "Egmond aan Zee"
	, country = NL
	, position = {lat = 52.606, lon = 4.419}
	}
  ,	{ name = "Prinses Amalia"
	, country = NL
	, position = {lat = 52.589444, lon = 4.206281}
	}
  ,	{ name = "Gemini"
	, country = NL
	, position = {lat = 54.036111, lon = 5.963056}
	}
  ,	{ name = "Dudgeon"
	, country = UK
	, position = {lat = 53.25, lon = 1.383333}
	}
  ,	{ name = "Horns Rev II"
	, country = DK
	, position = {lat = 55.6, lon = 7.59}
	}
/*
  ,	{ name = ""
	, country = NL
	, postion = {lat = , lon = }
	}
*/
  ]

ports :: [Port]
ports =
 [	{ Port
	| name = "Den Helder"
	, position = {lat = 52.9644, lon = 4.7889}
	}
 ,	{ Port
	| name = "Texel"
	, position = {lat = 53.0009, lon = 4.7897}
	}
 ,	{ Port
	| name = "Amsterdam"
	, position = {lat = 52.3774, lon = 4.8972}
	}
 ,	{ Port
	| name = "IJmuiden"
	, position = {lat = 52.4631, lon = 4.5804}
	}
 ,	{ Port
	| name = "Oude Schild"
	, position = {lat = 53.0432, lon = 4.8519}
	}
/*
 ,	{ Port
	| name = ""
	, position = {lat = , lon = }
	}
*/
 ]
 
 