definition module shipEvalDyn

import StdEnv, iTasks
import Control.Applicative, Data.Func, Data.Functor
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes //, ShipEval

:: Eval a = Eval (Env -> MaybeError String [a])

instance Functor Eval
instance pure Eval
instance <*>  Eval
instance Monad Eval
instance MonadFail Eval

guard :: Bool -> Eval ()

gen :: String [x] (Eval a) -> Eval a | TC x

:: Env :== String -> MaybeError String Dynamic
read :: String -> Eval a | TC a

new :: Env
tie :: String a Env -> Env | TC a

concatMaybeError :: [MaybeError s [a]] -> MaybeError s [a]
