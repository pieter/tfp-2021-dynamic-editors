module shipsTagged

import StdEnv, iTasks
import Control.Applicative,  Data.Func
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes, ShipEval
import iTasks.Extensions.Editors.DynamicEditor

valueOf :: !(DynamicEditor a) !(DynamicEditorValue a) -> ?a | TC a
valueOf de Undefined = ?None
valueOf de dev = ?Just $ valueCorrespondingTo de dev

Start :: !*World -> *World
Start world = doTasks editExpr world

:: Gen
	= Ship Name Gen
	| Park Name Gen
	| Port Name Gen
	| Cond Expr Gen
	| Ret Expr
:: Expr
	= LE Expr Expr
	| Eq Expr Expr // also Boolean Eq
	| Not Expr
	| And Expr Expr
	| Add Expr Expr
	| HasFlag Expr [Country]
	| HasName Expr [String]
	| HasKind Expr [Kind]
	| Name Expr
	| Var Name
	| Real Real
	| String String
	| Lit Port
	| Position Expr
	| Distance Expr Expr
//	| Gen Gen

:: Tagged a b =: Tagged a

derive class iTask Gen, Expr, IdType, Port

eq :: Res Res -> Eval Res
eq (ShipResult x)   (ShipResult y)   = pure (BoolResult (x == y))
eq (ParkResult x)   (ParkResult y)   = pure (BoolResult (x == y))
eq (BoolResult x)   (BoolResult y)   = pure (BoolResult (x == y))
eq (RealResult x)   (RealResult y)   = pure (BoolResult (x == y))
eq (StringResult x) (StringResult y) = pure (BoolResult (x == y))
eq _ _ = fail "Eq: cannot compare unequal types"

le :: Res Res -> Eval Res
le (RealResult x) (RealResult y) = pure (BoolResult (x < y))
le (StringResult x) (StringResult y) = pure (BoolResult (x < y))
le _ _ = fail "no instance of < for these values"

// Identical to ships2. fails shoud no longer occur
class eval a :: a -> Eval Res

instance eval Gen where
	eval e = case e of
		Ship name e = gen name ships (eval e)
		Park n e = gen n windParks (eval e)
		Port n e = gen n ports (eval e)
		Cond c e = eval c >>= \(BoolResult b). guard b >>| eval e
		Ret e = eval e

instance eval Expr where
	eval expr = case expr of
		Var name = read name
		Real real = pure (RealResult real)
		String s = pure (StringResult s)
		Lit p = pure (PortResult p)
		Distance x y = eval x >>= \a. eval y >>= \b.pure (RealResult (distance a b))
		Name e = eval e >>= \a.case a of ShipResult s = pure (StringResult s.Ship.name); ParkResult s = pure (StringResult s.Park.name); _ = fail "Name: this has no name"
		LE  x y = eval x >>= \a. eval y >>= \b.le a b
		Eq  x y = eval x >>= \a. eval y >>= \b.eq a b
		Not x   = eval x >>= \a. case a of (BoolResult a) = pure (BoolResult (not a)); _ = fail "Not: wrong arguments"
		And x y = eval x >>= \a. eval y >>= \b. case (a, b) of (BoolResult a, BoolResult b) = pure (BoolResult (a && b)); _ = fail "And: wrong arguments"
		Add x y = eval x >>= \a. eval y >>= \b. case (a, b) of (RealResult a, RealResult b) = pure (RealResult (a + b)); _ = fail "And: wrong arguments"
		HasFlag exp list = eval exp >>= \x.case x of ShipResult s = pure (BoolResult (isMember s.flag list)); _ = fail "HasFlag: Ship expected"
		HasName exp list = eval exp >>= \x.case x of
				ShipResult s = pure (BoolResult (or (map (matchName s.Ship.name) list)))
				ParkResult s = pure (BoolResult (or (map (matchName s.Park.name) list)))
				StringResult s = pure (BoolResult (or (map (matchName s) list)))
				_ = fail "HasName: Ship, Park or String expected"
		Position exp = eval exp >>= \x. case x of
				ShipResult s = pure $ PositionResult s.Ship.position
				PortResult p = pure $ PositionResult p.Port.position
				ParkResult p = pure $ PositionResult p.Park.position
				_ = fail "Position: Ship, Park or Port expected"
//		Gen g = eval g

run :: a -> MaybeError String [Res] | eval a
run f = mapMaybeError removeDup (g new) where (Eval g) = eval f

matchName :: String String -> Bool
matchName name patt = match [c \\ c <-: patt] [c \\ c <-: name]
where
	match [] _ = True
	match ['.':r] [c:x] = match r x
	match [p:r] [c:x] | toLower p == toLower c
		= match r x
		= False
	match _ _ = False

// ===== GUI

:: Bind a =  {idnt::String, val::a}
:: State :== [Bind Dynamic]
derive class iTask Bind
instance < (Bind a) where (<) x y = x.idnt < y.idnt

editExpr :: Task (State, DynamicEditorValue Gen)
editExpr = editStep Undefined
where
	editStep :: (DynamicEditorValue Gen) -> Task (State, DynamicEditorValue Gen)
	editStep expr1 =
		withShared state0 \stateParam ->
		withShared expr1 \dynValue ->
		(((   updateExpression (stateParam >*< dynValue) >&^ \sds.(runGen sds -|| identifierEditor stateParam) <<@ ArrangeHorizontal))
		)<<@ ArrangeHorizontal

	identifierEditor :: (SimpleSDSLens State) -> Task State
	identifierEditor sds =
	  (		Title "Identifiers" @>> 
	   		editChoiceWithShared [ChooseFromGrid showBinding] sds ?None)
	   ||- (Title "Add new identifier" @>> Hint "Identifier names must be unique" @>>
			forever
			  (get sds @ map (\b->b.idnt) >>- \vars ->
			   enterInformation [] >>*
			   [OnAction (Action "Add") (ifValue (\def -> not (isMember (def.idnt) vars))
				  (\def -> upd (\l -> sort [{idnt=def.idnt, val=idDyn def.val}: l]) sds))
			  ]))

	updateExpression :: (Shared a ([Bind Dynamic],DynamicEditorValue Gen)) -> Task ([Bind Dynamic],DynamicEditorValue Gen) | RWShared a
	updateExpression sds =
		Title ("Construct query") @>>
		updateSharedInformation 
	     	[UpdateSharedUsing
	     		id
	     		(\a v  -> v)
	     		(parametrisedDynamicEditor exprEditor)
	     	]
	     	sds

	runGen :: (a () (?([Bind Dynamic],DynamicEditorValue Gen)) ()) -> Task (?([Bind Dynamic],DynamicEditorValue Gen)) | RWShared a
	runGen sds = 
		Title "Result of current query" @>>
		viewSharedInformation [ViewAs \s.case s of
			?Just (state,Undefined) = [["-"]]
			?Just (state,gen) = case run (valueCorrespondingTo (exprEditor state) gen) of
				Error s = [[s]]
				Ok l = map (gText{|*|} AsSingleLine o ?Just) l
			?None = [["No result"]]] sds

showBinding :: (Bind Dynamic) -> Bind String
showBinding {idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}

:: IdType = ShipId | WindparkId | PortId

idDyn :: IdType -> Dynamic
idDyn ShipId	 = dynamic hd ships
idDyn WindparkId = dynamic hd windParks
idDyn PortId	 = dynamic hd ports

/*
Stress why we have a fixed number of Returns instead of select with a type selection.
Can we do this in two steps?
*/

T1 f (Tagged x) = Tagged (f x)
Tl f (Tagged x) y = Tagged (f x y)
T2 f (Tagged x) (Tagged y) = Tagged (f x y)

:: TExpr a :== Tagged Expr a

exprEditor :: State -> DynamicEditor Gen
exprEditor state = DynamicEditor
  [ DynamicConsGroup "Generators"
 	[ de "Ship"   (dynamic \(Tagged name).Ship name :: (Tagged String Ship) Gen -> Gen)
 	, de "Park"   (dynamic \(Tagged name).Park name :: (Tagged String Park) Gen -> Gen)
 	, de "Port"   (dynamic \(Tagged name).Port name :: (Tagged String Port) Gen -> Gen)
 	, de "Cond"   (dynamic \(Tagged expr).Cond expr :: (TExpr Bool) Gen -> Gen)
 	, de "Return" (dynamic \(Tagged e).Ret e :: A.a:(TExpr a) -> Gen)
 	]
  , DynamicConsGroup "Identifiers"
	[ de ("Var " + idnt) (dynamic (Tagged (Var idnt)) :: TExpr t) \\ {idnt,val=x::t} <- state]
  , DynamicConsGroup "Names"
	[ de ("Name " + idnt) (dynamic (Tagged idnt) :: Tagged String t) \\ {idnt,val=x::t}  <- state]
  , DynamicConsGroup "Known Ports"
	[ de ("Port " + p.Port.name) (dynamic Tagged (Position (Lit p)) :: TExpr Position) \\ p <- ports]
  , DynamicConsGroup "Expressions"
	[ de "Less" (dynamic T2 LE :: (TExpr Real) (TExpr Real) -> TExpr Bool)
		<<@ applyHorizontalLayout <<@ HideIfOnlyChoice
	, de "Eq" (dynamic T2 Eq :: A.a: (TExpr a) (TExpr a) -> TExpr Bool)
		<<@ applyHorizontalLayout <<@ HideIfOnlyChoice
	, de "And" (dynamic T2 And :: (TExpr Bool) (TExpr Bool) -> TExpr Bool)
		<<@ applyHorizontalLayout
	, de "Not" (dynamic T1 Not :: (TExpr Bool) -> TExpr Bool)
		<<@ applyHorizontalLayout
	, de "Has flag" (dynamic Tl HasFlag :: (TExpr Ship) [Country] -> TExpr Bool)
		<<@ applyHorizontalLayout
	, de "Has kind" (dynamic Tl HasKind :: (TExpr Ship) [Kind] -> TExpr Bool)
		<<@ applyHorizontalLayout
	, de "Has name" (dynamic Tl HasName :: (TExpr Ship) [String] -> TExpr Bool)
		<<@ applyHorizontalLayout
//		, de "In port" (dynamic \(Tagged e) (Tagged p).Tagged (LE (Distance e p) (Real 2.0)) :: (TExpr Ship) (TExpr Port) -> TExpr Bool) // This is special
	, de "Number" (dynamic \x.Tagged (Real x) :: Real -> TExpr Real)
		<<@ HideIfOnlyChoice
	, de "Distance" (dynamic T2 Distance :: (TExpr Position) (TExpr Position) -> TExpr Real)
		<<@ applyHorizontalLayout <<@ HideIfOnlyChoice
	, de "Ship pos" (dynamic T1 Position :: (TExpr Ship) -> TExpr Position)
	, de "Port pos" (dynamic T1 Position :: (TExpr Port) -> TExpr Position)
	, de "Park pos" (dynamic T1 Position :: (TExpr Park) -> TExpr Position)
	, de "Name constant" (dynamic (\s.Tagged (String s)) :: String -> TExpr String)
		<<@ HideIfOnlyChoice
	, de "Name ship"
		(dynamic T1 Name :: (TExpr Ship) -> TExpr String)
		<<@ HideIfOnlyChoice
	, de "Name windpark" (dynamic T1 Name :: (TExpr Park) -> TExpr String)
		<<@ HideIfOnlyChoice
	]
	, DynamicConsGroup "Editors"
		[ ce "Int"   intEditor    <<@ HideIfOnlyChoice
		, ce "Real"  realEditor    <<@ HideIfOnlyChoice
		, ce "Bool"  boolEditor   <<@ HideIfOnlyChoice
		, ce "String" stringEditor   <<@ HideIfOnlyChoice
		, ce "Countries" countriesEditor   <<@ HideIfOnlyChoice
		, ce "Country" countryEditor   <<@ HideIfOnlyChoice
		, ce "Kinds"  kindsEditor   <<@ HideIfOnlyChoice
		, ce "Names"  namesEditor   <<@ HideIfOnlyChoice
		]
	]

de name dyn = functionConsDyn name name dyn
ce name edt = customEditorCons name name edt
lc name edt = listCons name name edt

basicClasses      = [ "shiptask-base" ]
horizontalClasses = [ "shiptask-horizontal" ]
verticalClasses   = [ "shiptask-vertical" ]
boxedClasses      = [ "shiptask-boxed" ]

applyHorizontalBoxedLayout = ApplyCssClasses $ basicClasses ++ horizontalClasses ++ boxedClasses
applyVerticalBoxedLayout   = ApplyCssClasses $ basicClasses ++ verticalClasses ++ boxedClasses
applyHorizontalLayout      = ApplyCssClasses $ basicClasses ++ horizontalClasses
applyVerticalLayout        = ApplyCssClasses $ basicClasses ++ verticalClasses
addAllLabels list = AddLabels (map ?Just list)

:: EQ  a = EQ  ((Eval a)->Eval a) & == a
//:: Pos a = Pos ((Eval a)->Eval a) & toPosition a

toFunctionConsDyn :: (Bind Dynamic) -> DynamicCons
toFunctionConsDyn {idnt, val=val=:a::t}
	= functionConsDyn ("Var." +++ idnt) idnt (dynamic (Tagged (Var idnt)) :: (TExpr t)) <<@ HideIfOnlyChoice

toName :: (Bind Dynamic) -> DynamicCons
toName {idnt,val=x::t}     = functionConsDyn ("Name." +++ idnt) idnt (dynamic (Tagged idnt) :: Tagged String t)

intEditor :: Editor Int (?Int)
intEditor = gEditor{|*|} EditValue

realEditor :: Editor Real (?Real)
realEditor = gEditor{|*|} EditValue

boolEditor :: Editor Bool (?Bool)
boolEditor = gEditor{|*|} EditValue

stringEditor :: Editor String (?String)
stringEditor = gEditor{|*|} EditValue

countriesEditor :: Editor [Country] (?[Country])
countriesEditor = gEditor{|*|} EditValue

countryEditor :: Editor Country (?Country)
countryEditor = gEditor{|*|} EditValue

kindsEditor :: Editor [Kind] (?[Kind])
kindsEditor = gEditor{|*|} EditValue

namesEditor :: Editor [String] (?[String])
namesEditor = gEditor{|*|} EditValue

state0 :: State
state0 =
	[{idnt = "s", val = dynamic (hd ships)}
	,{idnt = "t", val = dynamic (hd ships)}
	,{idnt = "w", val = dynamic (hd windParks)}
	,{idnt = "v", val = dynamic (hd windParks)}
	,{idnt = "p", val = dynamic (hd ports)}
	]

instance + String where (+) s t = s +++ t



