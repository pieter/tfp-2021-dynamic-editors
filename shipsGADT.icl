module shipsGADT

import StdEnv, iTasks
import Control.Applicative, Data.Func, Data.Functor
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes, shipEvalDyn
import iTasks.Extensions.Editors.DynamicEditor

Start :: !*World -> *World
Start world = doTasks editExpr world

:: Gen					// discuss why we do nat have a Gen a
	= Ship Name Gen		// with a state we can make these implecit and just generate all variables in the state, less efficient and flexible
	| Park Name Gen
	| Port Name Gen
	| Cond (Expr Bool) Gen
	| E.a: Ret (Expr a) & type, toRes a
:: Expr a
	= E.b: LE (BM a Bool) (Expr b) (Expr b) & type, < b
	| E.b: Eq (BM a Bool) (Expr b) (Expr b) & type, == b // also Boolean Eq
	| Not (BM a Bool) (Expr Bool)
	| And (BM a Bool) (Expr Bool) (Expr Bool)
	| Add (Expr a) (Expr a) & type, + a
	| Arith (a a->a) (Expr a) (Expr a) // experimental
	| E.b: Comp (BM a Bool) (b b->Bool) (Expr b) (Expr b) // experimental
	| HasFlag (BM a Bool) (Expr Ship) [Country]
	| E.b: HasName (BM a Bool) (Expr b) [String] & type, name b
	| HasKind (BM a Bool) (Expr Ship) [Kind]
	| E.b: Name (BM a Name) (Expr b) & type, name b
	| Var Name & TC a
	| Lit a & type a
	| Distance (BM a Real) DistArg DistArg
//	| Gen Gen // not possible without a type argument
:: Name :== String

gDefault{|Gen|}       = abort "No gDefault for Gen"
gEq{|Gen|}        _ _ = abort "No gEq for Gen"
JSONEncode{|Gen|} _ _ = abort "No JSONEncode for Gen"
JSONDecode{|Gen|} _ _ = abort "No JSONDecode for Gen"
gText{|Gen|}      _ _ = abort "No gText for Gen"
gEditor{|Gen|}      _ = abort "No gEditor for Gen"


:: BM a b = {ab::a->b, ba::b->a}
bm = {ab=id, ba=id}
:: DistArg = E.a: DistArg (Expr a) & type, toPosition a
:: BinOp a = BinOp (a a->a)
class type t | toString t

class name a :: a -> Name
instance name Ship where name ship = ship.Ship.name
instance name Port where name port = port.Port.name
instance name Park where name park = park.Park.name

// type systems prevents runtime errors

evalGen :: Gen -> Eval Res
evalGen expr = case expr of
	Ship n g = gen n ships $ evalGen g
	Park n g = gen n windParks $ evalGen g
	Port n g = gen n ports $ evalGen g
	Cond e g = eval e >>= \b.guard b >>| evalGen g
	Ret e = fmap toRes $ eval e

eval :: (Expr a) -> Eval a
eval expr = case expr of
	LE bm x y = fmap bm.ba $ fmap (<) (eval x) <*> eval y
	Eq bm x y = fmap bm.ba $ fmap (==) (eval x) <*> eval y
	Not bm x = fmap (bm.ba o not) $ eval x
	And bm x y = eval x >>= \a. eval y >>= \b.pure $ bm.ba (a && b)
	Add x y = fmap (+) (eval x) <*> eval y
	Arith f x y = fmap f (eval x) <*> eval y
	Comp bm f x y = fmap bm.ba $ fmap f (eval x) <*> eval y
	HasFlag bm x y = fmap bm.ba $ eval x >>= \a.pure (isMember a.flag y)
	HasName bm x y = fmap bm.ba $ eval x >>= \a.pure $ or $ map (matchName $ name a) y
	HasKind bm x y = fmap bm.ba $ eval x >>= \a.pure (isMember a.kind y)
	Name bm x = fmap (bm.ba o name) $ eval x
	Distance bm x y = fmap bm.ba $ fmap distance (evalDist x) <*> evalDist y
	Var n = read n
	Lit a = pure a
	
evalDist :: DistArg -> Eval Position
evalDist (DistArg e) = fmap toPosition $ eval e

class unbound a :: a -> [String]
instance unbound (Expr a) where
	unbound e = case e of
		LE _ x y = unbound x ++ unbound y
		Eq _ x y = unbound x ++ unbound y
		Not _ x = unbound x
		And _ x y = unbound x ++ unbound y
		Add x y = unbound x ++ unbound y
		Arith f x y = unbound x ++ unbound y
		HasFlag _ x l = unbound x
		HasName _ x l = unbound x
		HasKind _ x l = unbound x
		Name _ x = unbound x
		Distance _ x y = unbound x ++ unbound y
		Var n = [n]
		_ = []
instance unbound DistArg where unbound (DistArg e) = unbound e
instance unbound Gen where
	unbound g = case g of
		Ship n g = filter ((<>) n) $ unbound g
		Park n g = filter ((<>) n) $ unbound g
		Port n g = filter ((<>) n) $ unbound g
		Cond e g = unbound e ++ unbound g
		Ret e = unbound e

fixBindings :: State Gen -> Gen
fixBindings state gen = foldl (\g v.foldl add g [b \\ b <- state | b.idnt==v]) gen (removeDup $ unbound gen)
where
	add gen {idnt,val=a::Ship} = Ship idnt gen
	add gen {idnt,val=a::Port} = Port idnt gen
	add gen {idnt,val=a::Park} = Park idnt gen
	add gen _ = gen
	

// ========================

:: Bind a =  {idnt::String, val::a}
:: State :== [Bind Dynamic]
derive class iTask Bind
instance < (Bind a) where (<) x y = x.idnt < y.idnt

:: IdType = ShipId | WindparkId | PortId

idDyn :: IdType -> Dynamic
idDyn ShipId	 = dynamic hd ships
idDyn WindparkId = dynamic hd windParks
idDyn PortId	 = dynamic hd ports

derive class iTask IdType
//:: Ext a = None | Ext (a a->a) (Expr a)
:: Ext a b = None | Ext a b

gDefault{|Ext|}       _ _ = abort "Internal error with gDefault of Ext"
gEq{|Ext|}        _ _ _ _ = abort "Internal error with gEq of Ext"
JSONEncode{|Ext|} _ _ _ _ = abort "Internal error with JSONEncode of Ext"
JSONDecode{|Ext|} _ _ _ _ = abort "Internal error with JSONDecode of Ext"
gText{|Ext|}      _ _ _ _ = abort "Internal error with gText of Ext"
gEditor{|Ext|}  _ _ _ _ _ _ _ = abort "Internal error with gEditor of Ext"

showBindings :: (State -> [Bind String])
showBindings = map (\{idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}) 

showBinding :: (Bind Dynamic) -> Bind String
showBinding {idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}

:: EqType = EqInt | EdBool | EqShip
derive class iTask EqType

:: VarName a = VarName String

editExpr :: Task (State, DynamicEditorValue Gen)
editExpr = editStep Undefined
where
	editStep :: (DynamicEditorValue Gen) -> Task (State, DynamicEditorValue Gen)
	editStep expr1 =
		withShared state0 \stateParam ->
		withShared expr1 \dynValue ->
		let sds = stateParam >*< dynValue in
		((updateExpression sds >&^ runGen) <<@ ArrangeHorizontal) -|| identifierEditor stateParam <<@ ArrangeHorizontal

	identifierEditor :: (Shared a State) -> Task State  | RWShared a
	identifierEditor sds =
	  (		Title "Identifiers" @>> 
	   		enterChoiceWithShared [ChooseFromGrid showBinding] sds)
	   ||- (Title "Add new identifier" @>> Hint "Identifier names must be unique" @>>
			forever
			  (get sds @ map (\b->b.idnt) >>- \vars ->
			   enterInformation [] >>*
			   [OnAction (Action "Add") (ifValue (\def -> not (isMember (def.idnt) vars))
				  (\def -> upd (\l -> sort [{idnt=def.idnt, val=idDyn def.val}: l]) sds))
			  ]))

	updateExpression :: (Shared a (State,DynamicEditorValue Gen)) -> Task (State,DynamicEditorValue Gen) | RWShared a
	updateExpression sds =
		Title ("Construct query") @>>
		updateSharedInformation [UpdateSharedUsing id (\a v -> v) (parametrisedDynamicEditor exprEditor)] sds // @! ()

	runGen :: (a () (? (State,DynamicEditorValue Gen)) b) -> Task (?(State,DynamicEditorValue Gen)) | RWShared a & TC b
	runGen sds = 
		Title "Result of current query" @>>
		viewSharedInformation [ViewAs \s.case s of
			?Just (state,Undefined) = [["-"]]
			?Just (state,gen) = case run (fixBindings state (valueCorrespondingTo (exprEditor state) gen)) of
				Ok [] =[["Empty result"]]
				Ok l = map (gText{|*|} AsSingleLine o ?Just) l
				Error s = [[s]]
			?None = [["No valid query"]]
			] sds

run :: Gen -> MaybeError String [Res]
run gen = mapMaybeError removeDup (f new) where (Eval f) = evalGen gen

/*
Stress why we have a fixed number of Returns instead of select with a type selection.
Can we do this in two steps?
*/
exprEditor :: State -> DynamicEditor Gen
exprEditor state = DynamicEditor
	[ DynamicConsGroup "Generators"
	 	[ de "Ship introduction" (dynamic \(VarName name).Ship name :: (VarName Ship) Gen -> Gen)
	 	, de "Windpark introduction" (dynamic \(VarName name).Park name :: (VarName Park) Gen -> Gen)
	 	, de "Port introduction" (dynamic \(VarName name).Port name :: (VarName Port) Gen -> Gen)
	 	, de "Return Ship" (dynamic Ret :: (Expr Ship) -> Gen)
	 	, de "Return Port" (dynamic Ret :: (Expr Port) -> Gen)
	 	, de "Return Windpark" (dynamic Ret :: (Expr Park) -> Gen)
	 	, de "Return name" (dynamic Ret :: (Expr Name) -> Gen)
	 	, de "Condition" (dynamic Cond)
	 	]
	, DynamicConsGroup "Expressions"
		[ de "smaller numbers" (dynamic LE bm :: (Expr Real) (Expr Real) -> Expr Bool) <<@ applyHorizontalBoxedLayout
		, de "smaller strings" (dynamic LE bm :: (Expr String) (Expr String) -> Expr Bool) <<@ applyHorizontalBoxedLayout
		, de "distance less" (dynamic \a b d.LE bm (Distance bm a b) d) <<@ applyHorizontalBoxedLayout <<@ addAllLabels ["from","to","limit"]// a convenient abbrivation
		, de "Equal numbers" (dynamic Eq bm :: (Expr Real) (Expr Real) -> Expr Bool)
		, de "Equal ships" (dynamic Eq bm :: (Expr Ship) (Expr Ship) -> Expr Bool)
		, de "Equal positions" (dynamic Eq bm :: (Expr Position) (Expr Position) -> Expr Bool)
		, de "Equal strings" (dynamic Eq bm :: (Expr String) (Expr String) -> Expr Bool)
		, de "Not" (dynamic Not bm)
		, de "And" (dynamic And bm)
		, de "Add numbers" (dynamic Add :: (Expr Real) (Expr Real) -> Expr Real)
//		, de "Artih" (dynamic \(BinOp f).Arith f :: (BinOp Real) (Expr Real) (Expr Real) -> Expr Real) <<@ applyHorizontalBoxedLayout
		, de "Artih+" (dynamic (\(BinOp f) x y ext.
				case ext of
					Ext (BinOp g) z = Arith g (Arith f x y) z
					None = Arith f x y) :: (BinOp Real) (Expr Real) (Expr Real) (Ext (BinOp Real) (Expr Real)) -> Expr Real) <<@ applyHorizontalBoxedLayout
//					None = Arith f x y) :: A.a: (BinOp a) (Expr a) (Expr a) (Ext (BinOp a) (Expr a)) -> Expr a) <<@ applyHorizontalBoxedLayout
		, de "No Ext" (dynamic None :: A.a b:Ext a b)
		, de "Ext" (dynamic Ext :: A.a b: a b -> Ext a b)
		, de "Add strings" (dynamic Add :: (Expr String) (Expr String) -> Expr String)
		, de "Ship has flag" (dynamic HasFlag bm) <<@ applyHorizontalBoxedLayout
		, de "Ship is of kind" (dynamic HasKind bm) <<@ applyHorizontalBoxedLayout
		, de "HasName ship" (dynamic HasName bm :: (Expr Ship) [String] -> Expr Bool) <<@ applyHorizontalBoxedLayout
		, de "HasName port" (dynamic HasName bm :: (Expr Port) [String] -> Expr Bool) <<@ applyHorizontalBoxedLayout
		, de "HasName windpark" (dynamic HasName bm :: (Expr Park) [String] -> Expr Bool) <<@ applyHorizontalBoxedLayout
		, de "Name ship" (dynamic Name bm :: (Expr Ship) -> Expr String)
		, de "Name port" (dynamic Name bm :: (Expr Port) -> Expr String)
		, de "Name windpark" (dynamic Name bm :: (Expr Park) -> Expr String)
		, de "Number" (dynamic Lit :: Real -> Expr Real)
		, de "String" (dynamic Lit :: String -> Expr String)
		, de "Position" (dynamic Lit :: Position -> Expr Position)
		, de "Distance" (dynamic Distance bm)
		, de "Distance ship" (dynamic DistArg :: (Expr Ship) -> DistArg)
		, de "Distance port" (dynamic DistArg :: (Expr Port) -> DistArg)
		, de "Distance windpark" (dynamic DistArg :: (Expr Park) -> DistArg)
		, de "Distance position" (dynamic DistArg o Lit :: Position -> DistArg)
		]
	, DynamicConsGroup "Arith operations"
		[ de "addition" (dynamic BinOp (+) :: BinOp Real)
		, de "subtraction" (dynamic BinOp (-) :: BinOp Real)
		, de "multiplication" (dynamic BinOp (*) :: BinOp Real)
		]
	, DynamicConsGroup "Variables"
		[de (" " + idnt) (dynamic (Var idnt) :: Expr t) \\ {idnt,val=x::t} <- state]
	, DynamicConsGroup "Names"
		[de ("Name: " + idnt) (dynamic (VarName idnt) :: VarName t) \\ {idnt,val=x::t} <- state]
	, DynamicConsGroup "Ports"
		[de ("Port: " + p.Port.name) (dynamic Lit p) \\ p <- ports]
	, DynamicConsGroup "Lists"
		[ listCons "List.Country" "List of countries" let id :: [Country] -> [Country]; id l = l in id
		, listCons "List.Kind" "List of ship kinds" let id :: [Kind] -> [Kind]; id l = l in id
		, listCons "List.String" "List of names" let id :: [String] -> [String]; id l = l in id
		]
	, DynamicConsGroup "Editors"
		[ customEditorCons "Int"    "enter integer value" intEditor    <<@ HideIfOnlyChoice
		, customEditorCons "Real"   "enter real value" realEditor    <<@ HideIfOnlyChoice
		, customEditorCons "Bool"   "enter boolean value" boolEditor   <<@ HideIfOnlyChoice
		, customEditorCons "Country" "enter Country" countryEditor   <<@ HideIfOnlyChoice
		, customEditorCons "Kinds"   "enter Kinds" kindEditor   <<@ HideIfOnlyChoice
		, customEditorCons "Names"   "enter Names" nameEditor   <<@ HideIfOnlyChoice
		]
	]

de name dyn = functionConsDyn name name dyn <<@ HideIfOnlyChoice

basicClasses      = [ "shiptask-base" ]
horizontalClasses = [ "shiptask-horizontal" ]
verticalClasses   = [ "shiptask-vertical" ]
boxedClasses      = [ "shiptask-boxed" ]

applyHorizontalBoxedLayout = ApplyCssClasses $ basicClasses ++ horizontalClasses ++ boxedClasses
applyVerticalBoxedLayout   = ApplyCssClasses $ basicClasses ++ verticalClasses ++ boxedClasses
applyHorizontalLayout      = ApplyCssClasses $ basicClasses ++ horizontalClasses
applyVerticalLayout        = ApplyCssClasses $ basicClasses ++ verticalClasses
addAllLabels list = AddLabels (map ?Just list)

toFunctionConsDyn :: (Bind Dynamic) -> DynamicCons
toFunctionConsDyn {idnt, val=val=:a::t}
	= functionConsDyn ("Var." +++ idnt) idnt (dynamic (var idnt val) :: (Eval t)) // <<@ HideIfOnlyChoice
var :: String a -> (Eval a) | TC a
var v _ = Eval \env.case env v of Ok (a::a^) = Ok [a]; _ = Error ("No proper binding for " +++ v)

toName :: (Bind Dynamic) -> DynamicCons
toName {idnt,val=x::t} = functionConsDyn ("Name." +++ idnt) idnt (dynamic (VarName idnt) :: VarName t)

intEditor :: Editor Int (?Int)
intEditor = gEditor{|*|} EditValue

realEditor :: Editor Real (?Real)
realEditor = gEditor{|*|} EditValue

boolEditor :: Editor Bool (?Bool)
boolEditor = gEditor{|*|} EditValue

countryEditor :: Editor Country (?Country)
countryEditor = gEditor{|*|} EditValue

kindEditor :: Editor Kind (?Kind)
kindEditor = gEditor{|*|} EditValue

nameEditor :: Editor String (?String)
nameEditor = gEditor{|*|} EditValue

positionEditor :: Editor Position (?Position)
positionEditor = gEditor{|*|} EditValue

state0 :: State
state0 =
	[{idnt = "s", val = dynamic (hd ships)}
	,{idnt = "t", val = dynamic (hd ships)}
	,{idnt = "w", val = dynamic (hd windParks)}
	,{idnt = "v", val = dynamic (hd windParks)}
	,{idnt = "p", val = dynamic (hd ports)}
	]

instance + String where (+) s t = s +++ t
