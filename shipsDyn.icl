module shipsDyn

import StdEnv, Data.Func, Text, iTasks, iTasks.Extensions.DateTime
import iTasks.Extensions.Editors.DynamicEditor
import Math.Geometry
import Control.Applicative, Data.Functor
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import ShipTypes, shipEvalDyn

Start :: !*World -> *World
Start world = doTasks editExpr world

//:: Typed a b =: Typed a
:: Val a = Val a
:: Gen = Gen (Eval Res)
:: Cond = Cond (Eval Bool) Gen
:: VarName a = VarName String
:: Bind a =  {idnt::String, val::a}
:: State :== [Bind Dynamic]
derive class iTask Val, Gen, Cond, Bind, Eval, Port // , Res, Typed
instance < (Bind a) where (<) x y = x.idnt < y.idnt

:: IdType = Ship | Windpark | Port
derive class iTask IdType

idDyn :: IdType -> Dynamic
idDyn Ship		= dynamic hd ships
idDyn Windpark	= dynamic hd windParks
idDyn Port      = dynamic hd ports

showBindings :: (State -> [Bind String])
showBindings = map (\{idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}) 

showBinding :: (Bind Dynamic) -> Bind String
showBinding {idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}

:: EqType = EqInt | EdBool | EqShip
derive class iTask EqType

:: ToRes = E.a: ToRes (a -> Res) & iTask a
// the generis from the iTask class
gDefault{|ToRes|}       = abort "Internal error with gDefault of ToRes"
gEq{|ToRes|}        _ _ = abort "Internal error with gEq of ToRes"
JSONEncode{|ToRes|} _ _ = abort "Internal error with JSONEncode of ToRes"
JSONDecode{|ToRes|} _ _ = abort "Internal error with JSONDecode of ToRes"
gText{|ToRes|}      _ _ = abort "Internal error with gText of ToRes"
gEditor{|ToRes|}      _ = abort "Internal error with gEditor of ToRes"

editExpr :: Task (State, DynamicEditorValue Gen)
editExpr = editStep Undefined
where
	editStep :: (DynamicEditorValue Gen) -> Task (State, DynamicEditorValue Gen)
	editStep expr1 =
		withShared state0 \stateParam ->
		withShared expr1 \dynValue ->
		let sds = stateParam >*< dynValue in
		((updateExpression sds >&^ runGen) <<@ ArrangeHorizontal) -|| identifierEditor stateParam <<@ ArrangeHorizontal

	identifierEditor :: (Shared a State) -> Task State  | RWShared a
	identifierEditor sds =
	  (		Title "Identifiers" @>> 
	   		enterChoiceWithShared [ChooseFromGrid showBinding] sds)
	   ||- (Title "Add new identifier" @>> Hint "Identifier names must be unique" @>>
			forever
			  (get sds @ map (\b->b.idnt) >>- \vars ->
			   enterInformation [] >>*
			   [OnAction (Action "Add") (ifValue (\def -> not (isMember (def.idnt) vars))
				  (\def -> upd (\l -> sort [{idnt=def.idnt, val=idDyn def.val}: l]) sds))
			  ]))

	updateExpression :: (Shared a (State,DynamicEditorValue Gen)) -> Task (State,DynamicEditorValue Gen) | RWShared a
	updateExpression sds =
		Title ("Construct query") @>>
		updateSharedInformation [UpdateSharedUsing id (\a v -> v) (parametrisedDynamicEditor exprEditor)] sds

	runGen :: (a () (? (State,DynamicEditorValue Gen)) b) -> Task (?(State,DynamicEditorValue Gen)) | RWShared a & TC b
	runGen sds = 
		Title "Result of current query" @>>
		viewSharedInformation [ViewAs \s.case s of
			?Just (state,Undefined) = [["-"]]
			?Just (state,gen) = case run (valueCorrespondingTo (exprEditor state) gen) of
				Ok l = map (gText{|*|} AsSingleLine o ?Just) l
				Error s = [[s]]
			?None = [["No valid query"]]
			] sds

/*
Stress why we have a fixed number of Returns instead of select with a type selection.
Can we do this in two steps?
*/
exprEditor :: State -> DynamicEditor Gen
exprEditor state = DynamicEditor
	[ DynamicConsGroup "Generators"
	 	[ functionConsDyn "Gen.Ship" "Ship introduction"
	 		(dynamic \(VarName name) (Gen e).Gen $ gen name ships e :: (VarName Ship) Gen -> Gen)
	 	, functionConsDyn "Gen.WindPark" "Windpark introduction"
	 		(dynamic \(VarName name) (Gen e).Gen $ gen name windParks e :: (VarName WindPark) Gen -> Gen)
	 	, functionConsDyn "Gen.Port" "Port introduction"
	 		(dynamic \(VarName name) (Gen e).Gen $ gen name ports e :: (VarName Port) Gen -> Gen)
	 	, functionConsDyn "Gen.Ret.Ship" "Return Ship"
	 		(dynamic \g.Gen $ g >>= \s.pure $ ShipResult s :: (Eval Ship) -> Gen)
	 	, functionConsDyn "Gen.Ret.Port" "Return Port"
	 		(dynamic \g.Gen $ g >>= \s.pure $ PortResult s :: (Eval Port) -> Gen)
	 	, functionConsDyn "Gen.Ret.WindPark" "Return Windpark"
	 		(dynamic \g.Gen $ g >>= \s.pure $ WindResult s :: (Eval WindPark) -> Gen)
	 	, functionConsDyn "Gen.Ret.Name" "Return name"
	 		(dynamic \g.Gen $ g >>= \s.pure $ StringResult s :: (Eval String) -> Gen)
	 	, functionConsDyn "Gen.Cond" "Condition"
	 		(dynamic \cond (Gen gen).Gen $ cond >>= \b. guard b >>| gen :: (Eval Bool) Gen -> Gen)
	 	]
	, DynamicConsGroup "Variables"
		(map toFunctionConsDyn state)
	, DynamicConsGroup "Names"
		(map toName state)
	, DynamicConsGroup "Ports"
		[functionConsDyn ("Port." +++ p.Port.name) p.Port.name (dynamic pure p :: Eval Port) \\ p <- ports]
	, DynamicConsGroup "Conditions"
		[ functionConsDyn "Cond.Less" "Less"
			(dynamic \x y = x >>= \a. y >>= \b.pure (a < b) :: (Eval Real) (Eval Real) -> Eval Bool)
/*		, functionConsDyn "Cond.Eq.Res" "Eq Res"
			(dynamic \(ToRes f) x y = (f <*> x) >>= \a. (f <*> y) >>= \b.pure (a == b) /*:: A.a: ToRes (Eval a) (Eval a) -> Eval Bool*/)
			<<@ applyHorizontalLayout
*/		, functionConsDyn "Cond.Eq.Real" "Eq number"
			(dynamic \x y = x >>= \a. y >>= \b.pure (a == b) :: (Eval Real) (Eval Real) -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Eq.Position" "Eq position"
			(dynamic \x y = fmap (==) x <*> y :: (Eval Position) (Eval Position) -> Eval Bool)
//			(dynamic \x y = x >>= \a. y >>= \b.pure (a == b) :: (Eval Position) (Eval Position) -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Eq.Ship" "Eq ship"
			(dynamic \x y = x >>= \a. y >>= \b.pure (a == b) :: (Eval Ship) (Eval Ship) -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
/*		, functionConsDyn "Cond.Eq" "Eq"
			(dynamic \(EQ f) x y = f x >>= \a. y >>= \b.pure (a == b) :: A.a: (EQ a) (Eval a) (Eval a) -> Eval Bool) // not so nice, can still fail, three cases is better
			<<@ applyHorizontalLayout
*/		, functionConsDyn "Cond.And" "And"
			(dynamic \x y = x >>= \a. y >>= \b.pure (a && b) :: (Eval Bool) (Eval Bool) -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Not" "Not"
			(dynamic \x = x >>= \a.pure (not a) :: (Eval Bool) -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Flag" "Has flag"
			(dynamic \ship list = ship >>= \s. pure (isMember s.flag list) :: (Eval Ship) [Country] -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Kind" "Has kind"
			(dynamic \ship list = ship >>= \s. pure (isMember s.kind list) :: (Eval Ship) [Kind] -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Name" "Has name"
			(dynamic \ship list = ship >>= \s. pure (or (map (matchName s.Ship.name) list)) :: (Eval Ship) [String] -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		, functionConsDyn "Cond.Port" "In port"
			(dynamic \ship port = ship >>= \s. port >>= \p.pure (distance s p < 1.0) :: (Eval Ship) (Eval Port) -> Eval Bool)
			<<@ applyHorizontalBoxedLayout
		]
	, DynamicConsGroup "Expressions"
		[ functionConsDyn "Expr.Real" "Number"
			(dynamic \real -> Eval \env.pure [real] :: Real -> Eval Real)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Expr.Distance" "Distance"
			(dynamic \x y = fmap distance x <*> y :: (Eval Position) (Eval Position) -> Eval Real)
			<<@ applyHorizontalLayout
			<<@ HideIfOnlyChoice
		, functionConsDyn "Expr.Name.String" "Name constant"
			(dynamic \s = pure s :: String -> Eval String)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Expr.Name.Ship" "Name ship"
			(dynamic \ship = ship >>= \s.pure s.Ship.name :: (Eval Ship) -> Eval String)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Expr.Name.WindPark" "Name windpark"
			(dynamic \park = park >>= \p.pure p.WindPark.name :: (Eval WindPark) -> Eval String)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Expr.Name.Port" "Name port"
			(dynamic \port = port >>= \p.pure p.Port.name :: (Eval Port) -> Eval String)
			<<@ HideIfOnlyChoice
		]
	, DynamicConsGroup "Positions"
		[ functionConsDyn "Position.Ship" "Position ship"
			(dynamic \e = e >>= \x.pure $ toPosition x :: (Eval Ship) -> Eval Position)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Position.Port" "Position port"
			(dynamic \e = e >>= \x.pure $ toPosition x :: (Eval Port) -> Eval Position)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Position.Park" "Position windpark"
			(dynamic \e = e >>= \x.pure $ toPosition x :: (Eval WindPark) -> Eval Position)
			<<@ HideIfOnlyChoice
		, functionConsDyn "Position.Position" "Position editor"
			(dynamic \e = pure e :: Position -> Eval Position)
			<<@ HideIfOnlyChoice
		, customEditorCons "Position.Position" "Position editor" positionEditor
		]
	, DynamicConsGroup "Lists"
		[ listCons "List.Country" "List of countries" let id :: [Country] -> [Country]; id l = l in id
		, listCons "List.Kind" "List of ship kinds" let id :: [Kind] -> [Kind]; id l = l in id
		, listCons "List.String" "List of names" let id :: [String] -> [String]; id l = l in id
		]
/*	, DynamicConsGroup "Types"
		[ functionConsDyn "EQ.Real" "Number" (dynamic EQ id :: EQ Real)
		, functionConsDyn "EQ.Name" "Name" (dynamic EQ id :: EQ String)
		, functionConsDyn "EQ.Position" "Position" (dynamic EQ id :: EQ Position)
		, functionConsDyn "Pos.Position" "Position" (dynamic Pos id :: Pos Position)
		, functionConsDyn "Pos.Ship" "Ship" (dynamic Pos id :: Pos Ship)
		, functionConsDyn "Pos.WindPark" "Windpark" (dynamic Pos id :: Pos WindPark)
		, functionConsDyn "Pos.Port" "Port" (dynamic Pos id :: Pos Port)
		]
*/	, DynamicConsGroup "Result Types"
		[ functionConsDyn "Ty.Real" "Real"
			(dynamic (ToRes RealResult) :: ToRes)
			<<@ applyHorizontalLayout
		, functionConsDyn "Ty.Bool" "Boolean"
			(dynamic (ToRes BoolResult) :: ToRes)
			<<@ applyHorizontalLayout
		, functionConsDyn "Ty.String" "String"
			(dynamic (ToRes StringResult) :: ToRes)
			<<@ applyHorizontalLayout
		]
	, DynamicConsGroup "Editors"
		[ customEditorCons "Int"    "enter integer value" intEditor    <<@ HideIfOnlyChoice
		, customEditorCons "Real"   "enter real value" realEditor    <<@ HideIfOnlyChoice
		, customEditorCons "Bool"   "enter boolean value" boolEditor   <<@ HideIfOnlyChoice
		, customEditorCons "Country" "enter Country" countryEditor   <<@ HideIfOnlyChoice
		, customEditorCons "Kinds"   "enter Kinds" kindEditor   <<@ HideIfOnlyChoice
		, customEditorCons "Names"   "enter Names" nameEditor   <<@ HideIfOnlyChoice
		]
	]

basicClasses      = [ "shiptask-base" ]
horizontalClasses = [ "shiptask-horizontal" ]
verticalClasses   = [ "shiptask-vertical" ]
boxedClasses      = [ "shiptask-boxed" ]

applyHorizontalBoxedLayout = ApplyCssClasses $ basicClasses ++ horizontalClasses ++ boxedClasses
applyVerticalBoxedLayout   = ApplyCssClasses $ basicClasses ++ verticalClasses ++ boxedClasses
applyHorizontalLayout      = ApplyCssClasses $ basicClasses ++ horizontalClasses
applyVerticalLayout        = ApplyCssClasses $ basicClasses ++ verticalClasses
addAllLabels list = AddLabels (map ?Just list)

//:: EQ  a = EQ  ((Eval a)->Eval a) & == a
//:: Pos a = Pos ((Eval a)->Eval a) & toPosition a
//:: POS a = POS (Eval a) & toPosition a

toFunctionConsDyn :: (Bind Dynamic) -> DynamicCons
toFunctionConsDyn {idnt, val=val=:a::t}
	= functionConsDyn ("Var." +++ idnt) idnt (dynamic (var idnt val) :: (Eval t)) // <<@ HideIfOnlyChoice
/*
toFunctionConsDyn {idnt, val=val=:a::Ship}
	= functionConsDyn ("Var." +++ idnt) idnt (dynamic (var idnt val) :: (Eval Ship)) <<@ HideIfOnlyChoice
toFunctionConsDyn {idnt, val=val=:a::WindPark}
	= functionConsDyn ("Var." +++ idnt) idnt (dynamic  (var idnt val) :: (Eval WindPark)) <<@ HideIfOnlyChoice
toFunctionConsDyn {idnt, val=val=:a::Port}
	= functionConsDyn ("Var." +++ idnt) idnt (dynamic (var idnt val) :: (Eval Port)) <<@ HideIfOnlyChoice
toFunctionConsDyn {idnt, val} = abort ("Name with unknown type: " +++ idnt)
*/

var :: String a -> (Eval a) | TC a
var v _ = Eval \env.case env v of Ok (a::a^) = Ok [a]; _ = Error ("No proper binding for " +++ v)

toName :: (Bind Dynamic) -> DynamicCons
toName {idnt,val=x::t} = functionConsDyn ("Name." +++ idnt) idnt (dynamic (VarName idnt) :: VarName t)
/*
toName {idnt,val=x::Ship}     = functionConsDyn ("Name." +++ idnt) idnt (dynamic (VarName idnt) :: VarName Ship)
toName {idnt,val=x::WindPark} = functionConsDyn ("Name." +++ idnt) idnt (dynamic (VarName idnt) :: VarName WindPark)
toName {idnt,val=x::Port}     = functionConsDyn ("Name." +++ idnt) idnt (dynamic (VarName idnt) :: VarName Port)
toName {idnt} = abort ("toName: var has unkown type: " +++ idnt)
*/

intEditor :: Editor Int (?Int)
intEditor = gEditor{|*|} EditValue

realEditor :: Editor Real (?Real)
realEditor = gEditor{|*|} EditValue

boolEditor :: Editor Bool (?Bool)
boolEditor = gEditor{|*|} EditValue

//stringEditor :: Editor String (?String)
//stringEditor = gEditor{|*|} EditValue

countryEditor :: Editor Country (?Country)
countryEditor = gEditor{|*|} EditValue

kindEditor :: Editor Kind (?Kind)
kindEditor = gEditor{|*|} EditValue

nameEditor :: Editor String (?String)
nameEditor = gEditor{|*|} EditValue

positionEditor :: Editor Position (?Position)
positionEditor = gEditor{|*|} EditValue

// =======

run :: Gen -> MaybeError String [Res]
run (Gen f) = mapMaybeError removeDup (g new) where (Eval g) = f

guard :: Bool -> Eval ()
guard cond = Eval \env.Ok (if cond [()] [])

gen :: String [x] (Eval a) -> Eval a | TC x
gen name list (Eval e) = Eval \env.scanForErrors [e (tie name x env) \\ x <- list]

scanForErrors :: [MaybeError s [a]] -> MaybeError s [a]
scanForErrors l = scan l []
where
	scan [] c = Ok c
	scan [Error s:r] c = Error s
	scan [Ok l:r] c = scan r (l ++ c)

// =====
/*
:: Eval a = Eval (Env -> MaybeError String [a])

instance Functor Eval where fmap f e = e >>= \x.pure (f x)
instance pure Eval where pure x = Eval \env. Ok [x]
instance <*>  Eval where (<*>) fs xs = fs >>= \f.xs >>= \x.pure (f x)
instance Monad Eval where
	bind (Eval e) f = Eval \env.
		case e env of
			Ok l = scanForErrors [let (Eval b) = f x in b env \\ x <- l]
			Error e = Error e
instance MonadFail Eval where fail m = Eval \_.Error m

:: Env :== String -> MaybeError String Dynamic

new :: Env
new = \n.Error ("No " +++ n)

tie :: String a Env -> Env | TC a
tie n r e = \m.if (n==m) (Ok (dynamic r)) (e m)
*/
// =====

state0 :: State
state0 =
	[{idnt = "s", val = dynamic (hd ships)}
	,{idnt = "t", val = dynamic (hd ships)}
	,{idnt = "w", val = dynamic (hd windParks)}
	,{idnt = "v", val = dynamic (hd windParks)}
	,{idnt = "p", val = dynamic (hd ports)}
	]

texelPos = {lat = 53.03933, lon = 4.85035}
denHelderPos = {lat = 52.95938, lon = 4.79614}
ijmuidenPos = {lat = 52.4631, lon = 4.5804}

