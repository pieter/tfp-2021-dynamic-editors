module gadtDE

import StdEnv, iTasks,  Data.Functor
import iTasks.Extensions.Editors.DynamicEditor

/*
// Added to DynamicEditor:
valueOf :: !(DynamicEditor a) !(DynamicEditorValue a) -> ?a | TC a
valueOf de Undefined = ?None
valueOf de dev = ?Just $ valueCorrespondingTo de dev

*/

Start :: !*World -> *World
Start world = doTasks (task True) world

task :: a -> Task (DynamicEditorValue (E a)) | iTask, toString a
task a = withShared Undefined \sds. (edit sds -|| view sds) <<@ ArrangeHorizontal

edit :: (SimpleSDSLens (DynamicEditorValue (E a))) -> Task (DynamicEditorValue (E a)) | iTask a
edit sds = Title ("Editor") @>> updateSharedInformation [UpdateSharedUsing id (\a v -> v) (dynamicEditor exprEditor)] sds

view :: (SimpleSDSLens (DynamicEditorValue (E a))) -> Task (DynamicEditorValue (E a)) | iTask, toString a
view sds = Title "Value" @>> viewSharedInformation [ViewAs viewFun] sds
where
	viewFun dev = case valueOf exprEditor dev of
		?Just a = toString (eval a state)
		_       = "No value"

exprEditor :: DynamicEditor (E t)
exprEditor = DynamicEditor
  [ DynamicConsGroup "Operators"
	[ de "Add"		(dynamic Add bm :: (E Real) (E Real) -> E Real)
	, de "And"		(dynamic And bm :: (E Bool) (E Bool) -> E Bool)
	, de "Eq"		(dynamic Eq  bm :: (E Real) (E Real) -> E Bool)
	]
  , DynamicConsGroup "Identifiers"
	[ de idnt (dynamic (Var idnt) :: E t) \\ {idnt,val=x::t} <- state]
  , DynamicConsGroup "Basic Editors"
	[ ce "Real value"	realLitEditor
	, ce "Bool value"	boolLitEditor
	]
  ]

:: Bind a =  {idnt :: String, val :: a}
:: State :== [Bind Dynamic]
derive class iTask Bind 

:: BM a b = {ab :: a -> b, ba :: b -> a}
bm = {ab = id, ba = id}

:: E a
	= Add (BM a Real) (E Real) (E Real)
	| And (BM a Bool) (E Bool) (E Bool)
	| Eq  (BM a Bool) (E Real) (E Real)
	| Lit a
	| Var String

eval :: (E a) State -> a | TC a
eval e s = case e of
	Add bm x y = bm.ba (eval x s +  eval y s)
	And bm x y = bm.ba (eval x s && eval y s)
	Eq  bm x y = bm.ba (eval x s == eval y s)
	Lit a = a
	Var n = read s n

read :: State String -> a | TC a
read [] n = abort ("Read: no binding for " +++ n)
read [{idnt,val=v::a^}:r] n | idnt == n
	= v
	= read r n
read [_:r] n = read r n

de name dyn = functionConsDyn name name dyn
ce name edt = customEditorCons name name edt
lc name edt = listCons name name edt

gDefault{|E|}       a = Lit a
gEq{|E|}        _ _ _ = abort "No gEq for E"
JSONEncode{|E|} f b (Lit a) = f b a
JSONEncode{|E|} f b e = abort "No JSONEncode for this E"
JSONDecode{|E|} f b l = (fmap Lit a,r) where (a,r) = f b l
//JSONEncode{|E|} _ _ _ = abort "No JSONEncode for E"
//JSONDecode{|E|} _ _ _ = abort "No JSONDecode for E"
gText{|E|}      _ _ _ = abort "No gText for E"
gEditor{|E|}  _ _ _ _ = abort "No gEditor for E"

intEditor :: Editor Int (?Int)
intEditor = gEditor{|*|} EditValue

realEditor :: Editor Real (?Real)
realEditor = gEditor{|*|} EditValue

realLitEditor :: Editor (E Real) (?(E Real))
realLitEditor = mapEditorRead (\(Lit r).r) (mapEditorWrite (fmap Lit) realEditor)

boolLitEditor :: Editor (E Bool) (?(E Bool))
boolLitEditor = mapEditorRead (\(Lit r).r) (mapEditorWrite (fmap Lit) boolEditor)

boolEditor :: Editor Bool (?Bool)
boolEditor = gEditor{|*|} EditValue

basicClasses      = ["edit-task-base"]
horizontalClasses = ["edit-task-horizontal"]
verticalClasses   = ["edit-task-vertical"]
boxedClasses      = ["edit-task-boxed"]

applyHorizontalBoxedLayout = ApplyCssClasses (basicClasses ++ horizontalClasses ++ boxedClasses)
applyVerticalBoxedLayout = ApplyCssClasses (basicClasses ++ verticalClasses ++ boxedClasses)

state :: State
state =
	[{idnt = "x", val = dynamic 3.14}
	,{idnt = "y", val = dynamic 42.0}
	,{idnt = "a", val = dynamic False}
	,{idnt = "b", val = dynamic True}
	]

test :: Bool
test = read state "a"

test2 :: Real
test2 = read state "y"

