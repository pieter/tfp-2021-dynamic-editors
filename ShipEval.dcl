definition module ShipEval

import iTasks
import Control.Applicative
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes

eq :: Res Res -> Eval Res
le :: Res Res -> Eval Res

:: Env
:: Name :== String

new :: Env
tie :: Name Res Env -> Env
read :: Name -> Eval Res

:: Eval a = Eval (Env -> MaybeError String [a])

ap :: ([a]->[b]) (Eval a) -> Eval b
instance Functor Eval
instance pure Eval
instance <*>  Eval
instance Monad Eval
instance MonadFail Eval

guard :: Bool -> Eval ()
gen :: Name [x] (Eval a) -> Eval a | toRes x

