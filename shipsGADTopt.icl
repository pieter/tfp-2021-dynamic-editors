module shipsGADTopt

import StdEnv, iTasks
import Control.Applicative, Data.Func, Data.Functor
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes, shipEvalDyn
import iTasks.Extensions.Editors.DynamicEditor

valueOf :: !(DynamicEditor a) !(DynamicEditorValue a) -> ?a | TC a
valueOf de Undefined = ?None
valueOf de dev = ?Just $ valueCorrespondingTo de dev

Start :: !*World -> *World
Start world = doTasks editExpr world

:: Gen					// discuss why we do nat have a Gen a
	= Ship Name Gen		// with a state we can make these implecit and just generate all variables in the state, less efficient and flexible
	| Park Name Gen
	| Port Name Gen
	| E.a: Gen (Expr Bool) (a->Res) (Expr a)
:: Expr a
	= Not (BM a Bool) (Expr Bool)
	| E.b: OneOf (BM a Bool) (b b->Bool) (Expr b) [b]
	| E.b: Oper (b b->a) (Expr b) (Expr b)
	| Flag (BM a Country) (Expr Ship)
	| Kind (BM a Kind) (Expr Ship)
	| E.b: Name (BM a Name) (Expr b) & type, name b
	| Var Name & TC a
	| Lit a & type a
	| Distance (BM a Real) PosArg PosArg
:: PosArg = E.a: PosArg (Expr a) & toPosition a
:: Ext b = Ext (BinOp b b) (Expr b)
:: BinOpExt a b :== (BinOp a b) (Expr a) (Expr a) [Ext b] -> Expr b
:: BinOp a b = BinOp (a a->b)
:: ToRes a = ToRes (a->Res) (Expr a)

binOp :: (BinOp a b) (Expr a) (Expr a) [Ext b] -> Expr b
binOp (BinOp f) a b l = foldr (\ (Ext (BinOp g) c) d.Oper g d c) (Oper f a b) l 

expOp :: (Expr b) [Ext b] -> Expr b
expOp e l = foldr (\ (Ext (BinOp g) c) d.Oper g d c) e l 

:: BM a b = {ab::a->b, ba::b->a}
bm = {ab=id, ba=id}
:: Name :== String

gDefault{|Gen|}       = abort "No gDefault for Gen"
gEq{|Gen|}        _ _ = abort "No gEq for Gen"
JSONEncode{|Gen|} _ _ = abort "No JSONEncode for Gen"
JSONDecode{|Gen|} _ _ = abort "No JSONDecode for Gen"
gText{|Gen|}      _ _ = abort "No gText for Gen"
gEditor{|Gen|}      _ = abort "No gEditor for Gen"


class type t | toString t

class name a :: a -> Name
instance name Ship where name ship = ship.Ship.name
instance name Port where name port = port.Port.name
instance name Park where name park = park.Park.name

// type systems prevents runtime errors

evalGen :: Gen -> Eval Res
evalGen expr = case expr of
	Ship n g = gen n ships $ evalGen g
	Park n g = gen n windParks $ evalGen g
	Port n g = gen n ports $ evalGen g
	Gen a f e = eval a >>= \b.guard b >>| fmap f (eval e)

eval :: (Expr a) -> Eval a
eval expr = case expr of
	Not bm x = fmap (bm.ba o not) $ eval x
	OneOf bm f x l = fmap bm.ba $ eval x >>= \a.pure $ or $ map (f a) l
	Oper f x y = fmap f (eval x) <*> eval y
	Distance bm x y = fmap bm.ba $ fmap distance (evalDist x) <*> evalDist y
	Var n = read n
	Lit a = pure a
	Flag bm x = fmap (bm.ba o \s.s.flag) (eval x)
	Kind bm x = fmap (bm.ba o \s.s.kind) (eval x)
	Name bm x = fmap (bm.ba o name) (eval x)
	
evalDist :: PosArg -> Eval Position
evalDist (PosArg e) = fmap toPosition $ eval e

class unbound a :: a -> [String]
instance unbound (Expr a) where
	unbound e = case e of
		Not _ x = unbound x
		OneOf bm f x l = unbound x
		Oper f x y = unbound x ++ unbound y
		Distance _ x y = unbound x ++ unbound y
		Flag _ x = unbound x
		Name _ x = unbound x
		Kind _ x = unbound x
		Var n = [n]
		Lit _ = []
instance unbound PosArg where unbound (PosArg e) = unbound e
instance unbound Gen where
	unbound g = case g of
		Ship n g = filter ((<>) n) $ unbound g
		Park n g = filter ((<>) n) $ unbound g
		Port n g = filter ((<>) n) $ unbound g
		Gen a f e = unbound a ++ unbound e

fixBindings :: State Gen -> Gen
fixBindings state gen = foldl (\g v.foldl add g [b \\ b <- state | b.idnt==v]) gen (removeDup $ unbound gen)
where
	add gen {idnt,val=a::Ship} = Ship idnt gen
	add gen {idnt,val=a::Port} = Port idnt gen
	add gen {idnt,val=a::Park} = Park idnt gen
	add gen _ = gen
	

// ========================

:: Bind a =  {idnt::String, val::a}
:: State :== [Bind Dynamic]
derive class iTask Bind
instance < (Bind a) where (<) x y = x.idnt < y.idnt

:: IdType = ShipId | WindparkId | PortId

idDyn :: IdType -> Dynamic
idDyn ShipId	 = dynamic hd ships
idDyn WindparkId = dynamic hd windParks
idDyn PortId	 = dynamic hd ports

derive class iTask IdType

showBindings :: (State -> [Bind String])
showBindings = map (\{idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}) 

showBinding :: (Bind Dynamic) -> Bind String
showBinding {idnt,val} = {idnt=idnt, val=toString (typeCodeOfDynamic val)}

:: VarName a = VarName String

editExpr :: Task (State, DynamicEditorValue Gen)
editExpr = editStep Undefined
where
	editStep :: (DynamicEditorValue Gen) -> Task (State, DynamicEditorValue Gen)
	editStep expr1 =
		withShared state0 \stateParam ->
		withShared expr1 \dynValue ->
		let sds = stateParam >*< dynValue in
		((updateExpression sds >&^ runGen) <<@ ArrangeHorizontal) -|| identifierEditor stateParam <<@ ArrangeHorizontal

	identifierEditor :: (Shared a State) -> Task State  | RWShared a
	identifierEditor sds =
	  (		Title "Identifiers" @>> 
	   		enterChoiceWithShared [ChooseFromGrid showBinding] sds)
	   ||- (Title "Add new identifier" @>> Hint "Identifier names must be unique" @>>
			forever
			  (get sds @ map (\b->b.idnt) >>- \vars ->
			   enterInformation [] >>*
			   [OnAction (Action "Add") (ifValue (\def -> not (isMember (def.idnt) vars))
				  (\def -> upd (\l -> sort [{idnt=def.idnt, val=idDyn def.val}: l]) sds))
			  ]))

	updateExpression :: (Shared a (State,DynamicEditorValue Gen)) -> Task (State, DynamicEditorValue Gen) | RWShared a
	updateExpression sds =
		Title ("Construct query") @>>
		updateSharedInformation [UpdateSharedUsing id (\a v -> v) (parametrisedDynamicEditor exprEditor)] sds

	runGen :: (a () (? (State,DynamicEditorValue Gen)) b) -> Task (?(State,DynamicEditorValue Gen)) | RWShared a & TC b
	runGen sds = 
		Title "Result of current query" @>>
		viewSharedInformation [ViewAs \s.case s of
			?Just (state,Undefined) = [["-"]]
			?Just (state,gen) = case run (fixBindings state (valueCorrespondingTo (exprEditor state) gen)) of
				Ok [] =[["Empty result"]]
				Ok l = map (gText{|*|} AsSingleLine o ?Just) l
				Error s = [[s]]
			?None = [["No valid query"]]
			] sds

run :: Gen -> MaybeError String [Res]
run gen = mapMaybeError removeDup (f new) where (Eval f) = evalGen gen

//:: HandleExt a :== (Expr a) (Ext (BinOp a a) (Expr a)) -> Expr a
/*
:: Extension a :== Ext (BinOp a a) (Expr a)

handleExt :: (Expr a) (Ext (BinOp a a) (Expr a)) -> Expr a
handleExt a (Ext (BinOp f) b) = Oper f a b
handleExt a None = a

binOp :: (BinOp a b) (Expr a) (Expr a) -> Expr b
binOp (BinOp f) a b = Oper f a b

:: BinOpExt a b :== (BinOp a b) (Expr a) (Expr a) (Extension b) -> Expr b

binOpExt :: (BinOp a b) (Expr a) (Expr a) (Extension b) -> Expr b
binOpExt (BinOp f) a b e = handleExt (Oper f a b) e
*/

exprEditor :: State -> DynamicEditor Gen
exprEditor state = DynamicEditor
  [ DynamicConsGroup "Generators"
 	[ de "Ship introduction" (dynamic \(VarName name) g.Ship name g :: (VarName Ship) Gen -> Gen)
 	, de "Windpark introduction" (dynamic \(VarName name) g.Park name g :: (VarName Park) Gen -> Gen)
 	, de "Port introduction" (dynamic \(VarName name) g.Port name g :: (VarName Port) Gen -> Gen)
 	, de "Query" (dynamic \l (ToRes f e).Gen l f e :: A.a: (Expr Bool) (ToRes a) -> Gen) <<@ UseAsDefault
 	]
  , DynamicConsGroup "Expressions"
	[ de "Not"     		(dynamic expOp o Not bm)
	, de "Comp Real"	(dynamic binOp :: BinOpExt Real Bool)   <<@ applyHorizontalBoxedLayout
	, de "Comp String"	(dynamic binOp :: BinOpExt String Bool) <<@ applyHorizontalBoxedLayout
	, de "Logic"		(dynamic binOp :: BinOpExt Bool Bool)   <<@ applyHorizontalBoxedLayout
	, de "Arith" 		(dynamic binOp :: BinOpExt Real Real)   <<@ applyHorizontalBoxedLayout
	, de "Flag in"		(dynamic OneOf bm (==) :: (Expr Country) [Country] -> Expr Bool)
	, de "Flag in ext"	(dynamic \x y.expOp (OneOf bm (==) x y) :: (Expr Country) [Country] [Ext Bool] -> Expr Bool)
	, de "Kind in"		(dynamic OneOf bm (==) :: (Expr Kind) [Kind] -> Expr Bool)
	, de "Select port"	(dynamic Lit   :: Port -> Expr Port) <<@ HideIfOnlyChoice
	, de "Distance"		(dynamic Distance bm)
	, de "Ship pos"		(dynamic PosArg :: (Expr Ship) -> PosArg)
	, de "Port pos"		(dynamic PosArg :: (Expr Port) -> PosArg)
	, de "Park pos"		(dynamic PosArg :: (Expr Park) -> PosArg)
	, de "Position"		(dynamic PosArg o Lit :: Position -> PosArg)
	, de "Flag"			(dynamic Flag bm)
	, de "Kind"			(dynamic Kind bm)
	, de "Name ship"	(dynamic Name bm :: (Expr Ship) -> Expr String) <<@ UseAsDefault
	, de "Name port"	(dynamic Name bm :: (Expr Port) -> Expr String)
	, de "Name windpark"(dynamic Name bm :: (Expr Park) -> Expr String)
	, de "Number"		(dynamic Lit :: Real -> Expr Real)
	, de "String"		(dynamic Lit :: String -> Expr String)
	, de "Boolean"		(dynamic Lit :: Bool -> Expr Bool) <<@ UseAsDefault
	, de "Position "	(dynamic Lit :: Position -> Expr Position)
	, de "Ext"			(dynamic Ext :: A.b: (BinOp b b) (Expr b) -> Ext b)
	]
  , DynamicConsGroup "Result type"
	[ de "Ship result"	(dynamic ToRes toRes :: (Expr Ship) -> ToRes Ship)
	, de "Port result"	(dynamic ToRes toRes :: (Expr Port) -> ToRes Port)
	, de "Name result"	(dynamic ToRes toRes :: (Expr Name) -> ToRes Name) <<@ UseAsDefault
	, de "Park result"	(dynamic ToRes toRes :: (Expr Park) -> ToRes Park)
	]
  , DynamicConsGroup "Operations"
	[ de "add"			(dynamic BinOp (+) :: BinOp Real Real)
	, de "subtract"		(dynamic BinOp (-) :: BinOp Real Real)
	, de "times"		(dynamic BinOp (*) :: BinOp Real Real)
	, de "smaller"		(dynamic BinOp (<) :: BinOp Real Bool)  <<@ UseAsDefault
	, de "greater"		(dynamic BinOp (>) :: BinOp Real Bool)
	, de "equal real"	(dynamic BinOp (==) :: BinOp Real Bool)
	, de "equal string"	(dynamic BinOp (==) :: BinOp String Bool)
	, de "matches"		(dynamic BinOp matchName :: BinOp String Bool)
	, de "and"			(dynamic BinOp (&&) :: BinOp Bool Bool)  <<@ UseAsDefault
	, de "or"			(dynamic BinOp (||) :: BinOp Bool Bool)
	]
  , DynamicConsGroup "Variables"
	[de (" " + idnt) (dynamic (Var idnt) :: Expr t) \\ {idnt,val=x::t} <- state]
  , DynamicConsGroup "Names"
	[de ("Name: " + idnt) (dynamic (VarName idnt) :: VarName t) \\ {idnt,val=x::t} <- state]
  , DynamicConsGroup "Ports"
	[de ("Port: " + p.Port.name) (dynamic p) \\ p <- ports]
  , DynamicConsGroup "Lists"
	( map (\c.c  <<@ HideIfOnlyChoice)
	[ lc "Countries"	let id :: [Country] -> [Country]; id l = l in id
	, lc "Kinds"		let id :: [Kind] -> [Kind]; id l = l in id
	, lc "Patterns"		let id :: [String] -> [String]; id l = l in id
	, lc "Ext Bool"		let id :: [Ext Bool] -> [Ext Bool]; id l = l in id
	, lc "Ext Real"		let id :: [Ext Real] -> [Ext Real]; id l = l in id
	])
  , DynamicConsGroup "Editors"
	[ ce "Real value"	realEditor    <<@ HideIfOnlyChoice
	, ce "Bool value"	boolEditor    <<@ HideIfOnlyChoice
	, ce "Select Country" countryEditor <<@ HideIfOnlyChoice
	, ce "Select Kind"	kindEditor    <<@ HideIfOnlyChoice
	, ce "Name value"	nameEditor    <<@ HideIfOnlyChoice
	]
  ]

de name dyn = functionConsDyn name name dyn <<@ HideIfOnlyChoice
ce name edt = customEditorCons name name edt
lc name edt = listCons name name edt

basicClasses      = [ "shiptask-base" ]
horizontalClasses = [ "shiptask-horizontal" ]
verticalClasses   = [ "shiptask-vertical" ]
boxedClasses      = [ "shiptask-boxed" ]

applyHorizontalBoxedLayout = ApplyCssClasses $ basicClasses ++ horizontalClasses ++ boxedClasses
applyVerticalBoxedLayout   = ApplyCssClasses $ basicClasses ++ verticalClasses ++ boxedClasses
applyHorizontalLayout      = ApplyCssClasses $ basicClasses ++ horizontalClasses
applyVerticalLayout        = ApplyCssClasses $ basicClasses ++ verticalClasses
addAllLabels list = AddLabels (map ?Just list)

realEditor :: Editor Real (?Real)
realEditor = gEditor{|*|} EditValue

boolEditor :: Editor Bool (?Bool)
boolEditor = gEditor{|*|} EditValue

countryEditor :: Editor Country (?Country)
countryEditor = gEditor{|*|} EditValue

kindEditor :: Editor Kind (?Kind)
kindEditor = gEditor{|*|} EditValue

nameEditor :: Editor String (?String)
nameEditor = gEditor{|*|} EditValue

positionEditor :: Editor Position (?Position)
positionEditor = gEditor{|*|} EditValue

state0 :: State
state0 =
	[{idnt = "s", val = dynamic (hd ships)}
	,{idnt = "t", val = dynamic (hd ships)}
	,{idnt = "w", val = dynamic (hd windParks)}
	,{idnt = "v", val = dynamic (hd windParks)}
	,{idnt = "p", val = dynamic (hd ports)}
	]

instance + String where (+) s t = s +++ t
