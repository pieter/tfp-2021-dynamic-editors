module ships2

import iTasks
import Control.Applicative, Data.Functor
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes, ShipEval
//import qualified StdList

//Start w = doTasks myTask  w
//Start = distance texelPos denHelderPos
//Start = map run [e0, e1, e2]
Start w = doTasks genTask  w

genTask :: Task Gen
genTask = 
	withShared e2 \sds.
		(Title "Editor" @>> updateSharedInformation [] sds) -||
		(Title "Result" @>> viewSharedInformation [ViewAs run] sds) <<@ ArrangeHorizontal
european :: Country -> Bool
european land = isMember land [NL, DE, FR, BE, DK]

derive class iTask /* Ship, Park, Kind, Position, Country, Status, Res, */ Gen, Cond, Expr

shipsTask :: Task [Ship]
shipsTask = enterInformation []

myTask = shipsTask >>! \l. Title "Count" @>> viewInformation [ViewAs length] l

// =====

:: Gen
	= Ship Name Gen
	| Park Name Gen
	| Port Name Gen
	| Cond Cond Gen
	| Ret Expr
:: Cond
	= LE Expr Expr
	| Eq Expr Expr // no Boolean Eq
	| Not Cond
	| And Cond Cond
	| HasFlag Expr [Country]
	| HasName Expr [String]
	| HasKind Expr [Kind]
:: Expr
	= Var  Name
	| Num  Real
	| Add  Expr Expr
	| Distance Expr Expr
	| Name Expr
	| Gen  Gen

class eval a :: a -> Eval Res

instance eval Gen where
	eval e = case e of
		Ship name e = gen name ships (eval e)
		Park n e = gen n windParks (eval e)
		Port n e = gen n ports (eval e)
		Cond c e = eval c >>= \(BoolResult b). guard b >>| eval e
		Ret e = eval e

instance eval Cond where
	eval cond = case cond of
		LE  x y = eval x >>= \a.eval y >>= \b.le a b
		Eq  x y = eval x >>= \a.eval y >>= \b.eq a b
		Not x   = eval x >>= \a. case a of (BoolResult a) = pure (BoolResult (not a)); _ = fail "Not: wrong arguments"
		And x y = eval x >>= \a. eval y >>= \b. case (a, b) of (BoolResult a, BoolResult b) = pure (BoolResult (a && b)); _ = fail "And: wrong arguments"
		HasFlag exp list = eval exp >>= \x.case x of ShipResult s = pure (BoolResult (isMember s.flag list)); _ = fail "Flag: Ship expected"
		HasKind exp list = eval exp >>= \x.case x of ShipResult s = pure (BoolResult (isMember s.kind list)); _ = fail "Kind: Ship expected"
		HasName exp list = eval exp >>= \x.case x of
				ShipResult s = pure (BoolResult (or (map (matchName s.Ship.name) list)))
				ParkResult s = pure (BoolResult (or (map (matchName s.Park.name) list)))
				StringResult s = pure (BoolResult (or (map (matchName s) list)))
				_ = fail "Name: Ship, Park or String expected"

instance eval Expr where
	eval expr = case expr of
		Var name = read name
		Num real = pure (RealResult real)
		Distance x y = eval x >>= \a. eval y >>= \b.pure (RealResult (distance a b))
		Add x y = eval x >>= \a. eval y >>= \b.case (a, b) of
						(RealResult v, RealResult w) = pure (RealResult (v + w))
						_ = fail "Add needs two real arguments"
		Name e = eval e >>= \a.case a of
						ShipResult s = pure (StringResult s.Ship.name)
						PortResult s = pure (StringResult s.Port.name)
						ParkResult s = pure (StringResult s.Park.name)
						_ = fail "Name: this has no name"
		Gen g = eval g

//run :: a -> MaybeError String [Res] | eval a
//run :: a -> MaybeError String [String] | eval a
//run f = mapMaybeError ((gText{|*|} AsSingleLine o ?Just) o removeDup) (g new) where (Eval g) = eval f

run :: a -> [[String]] | eval a
run f = case mapMaybeError removeDup (g new) of
	Ok l = map (gText{|*|} AsSingleLine o ?Just) l
	Error s = [[s]]
where (Eval g) = eval f

// =====

e0 :: Gen
e0 = Ship s (Ret (Var s))
s = "s"

e1 :: Gen
e1 = Ship s (Ret (Name (Var s)))

e2 :: Gen
e2 = Ship s (Cond (HasFlag (Var s) [NL,UK]) (Ret (Name (Var s))))

// =====

texelPos = {lat = 53.03933, lon = 4.85035}
denHelderPos = {lat = 52.95938, lon = 4.79614}




