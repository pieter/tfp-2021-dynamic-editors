implementation module ShipEval

import iTasks
import Control.Applicative
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes

eq :: Res Res -> Eval Res
eq (ShipResult x)   (ShipResult y)   = pure (BoolResult (x == y))
eq (ParkResult x)   (ParkResult y)   = pure (BoolResult (x == y))
eq (BoolResult x)   (BoolResult y)   = pure (BoolResult (x == y))
eq (RealResult x)   (RealResult y)   = pure (BoolResult (x == y))
eq (StringResult x) (StringResult y) = pure (BoolResult (x == y))
eq _ _ = fail "Eq: cannot compare unequal types"

le :: Res Res -> Eval Res
le (RealResult x) (RealResult y) = pure (BoolResult (x < y))
le (StringResult x) (StringResult y) = pure (BoolResult (x < y))
le _ _ = fail "no instance of < for these values"

:: Env :== Name -> MaybeError String [Res]

new :: Env
new = \n.Error ("No " +++ n)

tie :: Name Res Env -> Env
tie n r e = \m.if (n==m) (Ok [r]) (e m)

read :: Name -> Eval Res
read name = Eval \env.env name

ap :: ([a]->[b]) (Eval a) -> Eval b
ap f (Eval g) = Eval \e.mapMaybeError f (g e)

instance Functor Eval where fmap f e = e >>= \x.pure (f x)
instance pure Eval where pure x = Eval \env. Ok [x]
instance <*>  Eval where (<*>) fs xs = fs >>= \f.xs >>= \x.pure (f x)
instance Monad Eval where
	bind (Eval e) f = Eval \env.
		case e env of
			Ok l = concatMaybeError [let (Eval b) = (f x) in b env \\ x <- l]
			Error e = Error e
instance MonadFail Eval where fail m = Eval \_.Error m

guard :: Bool -> Eval ()
guard cond = Eval \env.Ok (if cond [()] [])

gen :: Name [x] (Eval a) -> Eval a | toRes x
gen name list (Eval e) = Eval \env.concatMaybeError [e (tie name (toRes x) env) \\ x <- list]

concatMaybeError :: [MaybeError s [a]] -> MaybeError s [a]
concatMaybeError l = scan l []
where
	scan [] c = Ok c
	scan [Error s:r] c = Error s
	scan [Ok l:r] c = scan r (l ++ c)

/*
matchName :: String String -> Bool
matchName name patt = match [c \\ c <-: patt] [c \\ c <-: name]
where
	match [] _ = True
	match ['.':r] [c:x] = match r x
	match [p:r] [c:x] | toLower p == toLower c
		= match r x
		= False
	match _ _ = False
*/
