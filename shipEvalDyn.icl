implementation module shipEvalDyn

import StdEnv, iTasks
import Control.Applicative, Data.Func, Data.Functor
from Control.Monad import class Monad (bind,>>=,>>|)
from Control.Monad.Fail import class MonadFail(..)
import _SystemArray
import ShipTypes //, ShipEval

instance Functor Eval where fmap f e = e >>= \x.pure (f x)
instance pure Eval where pure x = Eval \env. Ok [x]
instance <*>  Eval where (<*>) fs xs = fs >>= \f.xs >>= \x.pure (f x)
instance Monad Eval where
	bind (Eval e) f = Eval \env.
		case e env of
			Ok l = concatMaybeError [let (Eval b) = f x in b env \\ x <- l]
			Error e = Error e
instance MonadFail Eval where fail m = Eval \_.Error m

guard :: Bool -> Eval ()
guard cond = Eval \env.Ok (if cond [()] [])

gen :: String [x] (Eval a) -> Eval a | TC x
gen name list (Eval e) = Eval \env.concatMaybeError [e (tie name x env) \\ x <- list]

new :: Env
new = \n.Error ("No " +++ n)

tie :: String a Env -> Env | TC a
tie n r e = \m.if (n==m) (Ok (dynamic r)) (e m)

read :: String -> Eval a | TC a
read n = Eval \env.case env n of Ok (a::a^) = Ok [a]; _ = Error ("No proper binding for " +++ n)

concatMaybeError :: [MaybeError s [a]] -> MaybeError s [a]
concatMaybeError l = scan l []
where
	scan [] c = Ok c
	scan [Error s:r] c = Error s
	scan [Ok l:r] c = scan r (l ++ c)
