module exprDE

import StdEnv, iTasks,  Data.Functor
import iTasks.Extensions.Editors.DynamicEditor

/*
// Added to DynamicEditor:
valueOf :: !(DynamicEditor a) !(DynamicEditorValue a) -> ?a | TC a
valueOf de Undefined = ?None
valueOf de dev = ?Just $ valueCorrespondingTo de dev

*/

Start :: !*World -> *World
Start world = doTasks (task2 True) world

task1 :: a -> Task (DynamicEditorValue a) | iTask, TC a
task1 _ = (Title ("Editor") @>> edit1 >&^ \s.Title "Value" @>> view1 s) <<@ ArrangeHorizontal

edit1 :: Task (DynamicEditorValue a) | iTask, TC a
edit1 = enterInformation [EnterUsing id (dynamicEditor exprEditor)]

view1 :: !(a () (? (DynamicEditorValue b)) c) -> Task (? (DynamicEditorValue b)) | TC c & RWShared a & iTask b
view1 sds = viewSharedInformation [ViewAs (fmap (valueCorrespondingTo exprEditor))] sds

task :: a -> Task (DynamicEditorValue a) | iTask, toString a
task a = withShared Undefined \sds. (edit sds -|| view sds) <<@ ArrangeHorizontal

edit :: (SimpleSDSLens (DynamicEditorValue a)) -> Task (DynamicEditorValue a) | iTask a
edit sds = Title ("Editor") @>> updateSharedInformation [UpdateSharedUsing id (\a v -> v) (dynamicEditor exprEditor)] sds

view :: (SimpleSDSLens (DynamicEditorValue a)) -> Task (DynamicEditorValue a) | iTask, toString a
view sds = Title "Value" @>> viewSharedInformation [ViewAs viewFun] sds
where viewFun dev =  case valueOf exprEditor dev of
		?None = "No value"
		?Just a = toString a

task2 :: a -> Task (DynamicEditorValue (Expr a)) | iTask, toString a
task2 a = withShared Undefined \sds. (edit2 sds -|| view2 sds) <<@ ArrangeHorizontal

edit2 :: (SimpleSDSLens (DynamicEditorValue (Expr a))) -> Task (DynamicEditorValue (Expr a)) | iTask a
edit2 sds = Title ("Editor") @>> updateSharedInformation [UpdateSharedUsing id (\a v -> v) (dynamicEditor exprEditor2)] sds

editTask ::(DynamicEditorValue (Expr a)) -> Task (DynamicEditorValue (Expr a)) | iTask a
editTask e = updateInformation [UpdateUsing id (\a v -> v) (dynamicEditor exprEditor2)] e

editor ::Task (DynamicEditorValue (Expr a)) | iTask a
editor = enterInformation [EnterUsing id (dynamicEditor exprEditor2)]

view2 :: (SimpleSDSLens (DynamicEditorValue (Expr a))) -> Task (DynamicEditorValue (Expr a)) | iTask, toString a
view2 sds = Title "Value" @>> viewSharedInformation [ViewAs viewFun] sds
where viewFun dev =  case valueOf exprEditor2 dev of
		?None = "No value"
		?Just a = toString (eval a)

exprEditor :: DynamicEditor a | TC a
exprEditor = DynamicEditor
	[ DynamicConsGroup "Functions"
		[ de "+ Int"   (dynamic (+) :: Int Int -> Int)   <<@ applyHorizontalBoxedLayout
		, de "- Int"   (dynamic (-) :: Int Int -> Int)   <<@ applyHorizontalBoxedLayout
		, de "== Int"  (dynamic (==) :: Int Int -> Bool) <<@ applyHorizontalBoxedLayout
		]
	, DynamicConsGroup "Peano"
		[ de "Succ"   (dynamic Succ)
		, de "Zero"   (dynamic Zero)
		, de "+ Num"  (dynamic (+)  :: Num Num -> Num)  <<@ applyHorizontalBoxedLayout
		, de "== Num" (dynamic (==) :: Num Num -> Bool) <<@ applyHorizontalBoxedLayout
		, de "mixed"  (dynamic \n i->toInt n == i :: Num Int -> Bool) <<@ applyHorizontalBoxedLayout
		]
/*	, DynamicConsGroup "overloading"
		[ de "== a"   (dynamic (==) :: A.t: t t -> Bool | == t) <<@ applyVertitalBoxedLayout 
		, de "== p"   (dynamic \(Comp f).f :: A.t: (Comp t) t t -> Bool) <<@ applyVertitalBoxedLayout
		, de "Comp Int" (dynamic \(Comp f).f :: (Comp Int) Int Int -> Bool)
		, de "Eq Int" (dynamic Comp (==) :: Comp Int)
		, de "LE Int" (dynamic Comp (<)  :: Comp Int)
		, de "Eq Num" (dynamic Comp (==) :: Comp Num)
		]
*/	, DynamicConsGroup "Basic Editors"
		[ ce "Int value" intEditor
		, ce "Bool value" boolEditor
//		, ce "Num value" numEditor
		]
	]
:: Comp t = Comp (t t->Bool)

:: Num = Zero | Succ Num
instance + Num where
	(+) Zero m = m
	(+) (Succ n) m = n + Succ m
instance == Num where
	(==) Zero Zero = True
	(==) (Succ n) (Succ m) = n == m
	(==) _ _ = False
instance toInt Num where
	toInt Zero = 0
	toInt (Succ n) = inc (toInt n)

:: Expr a
	= E.b: Oper (b b->a) (Expr b) (Expr b)
	| Lit a 

eval :: (Expr a) -> a
eval (Oper f x y) = f (eval x) (eval y)
eval (Lit a) = a
/*
exprEditor2 :: DynamicEditor (Expr t) | TC t
exprEditor2 = DynamicEditor
  [ DynamicConsGroup "Functions"
	[ de "Logic"		(dynamic binOpExt1 :: BinOpExt1 Bool Bool)	<<@ applyVerticalBoxedLayout
	, de "Arith"		(dynamic binOpExt1 :: BinOpExt1 Real Real)	<<@ applyHorizontalBoxedLayout
	, de "Comparison"	(dynamic binOpExt1 :: BinOpExt1 Real Bool)	<<@ applyHorizontalBoxedLayout
	, de "No Ext"		(dynamic None     :: A.a b:Ext1 a b) 		<<@ UseAsDefault
	, de "Ext"			(dynamic Ext1      :: A.a b: a b -> Ext1 a b)
	]
  , DynamicConsGroup "Operations"
	[ de "add"			(dynamic BinOp (+)  :: BinOp Real Real)  <<@ UseAsDefault
	, de "subtract"		(dynamic BinOp (-)  :: BinOp Real Real)
	, de "multiply" 	(dynamic BinOp (*)  :: BinOp Real Real)
	, de "smaller"		(dynamic BinOp (<)  :: BinOp Real Bool)  <<@ UseAsDefault
	, de "greater"		(dynamic BinOp (>)  :: BinOp Real Bool)
	, de "equal real"	(dynamic BinOp (==) :: BinOp Real Bool)
	, de "equal bool"	(dynamic BinOp (==) :: BinOp Bool Bool)
	, de "and"			(dynamic BinOp (&&) :: BinOp Bool Bool)  <<@ UseAsDefault
	, de "or"			(dynamic BinOp (||) :: BinOp Bool Bool)
	]
  , DynamicConsGroup "Basic Editors"
	[ ce "Real value"	realLitEditor
	, ce "Bool value"	boolLitEditor
	]
  ]
*/
exprEditor2 :: DynamicEditor (Expr t) | TC t
exprEditor2 = DynamicEditor
  [ DynamicConsGroup "Functions"
	[ de "Logic"		(dynamic binOp :: BinOpExt Bool Bool)	<<@ applyVerticalBoxedLayout
	, de "Arith"		(dynamic binOp :: BinOpExt Real Real)	<<@ applyVerticalBoxedLayout
	, de "Comparison"	(dynamic binOp :: BinOpExt Real Bool)	<<@ applyHorizontalBoxedLayout
	, de "Ext"			(dynamic Ext    :: A.b: (BinOp b b) (Expr b) -> Ext b)  <<@ HideIfOnlyChoice
	]
  , DynamicConsGroup "Operations"
	[ de "add"			(dynamic BinOp (+)  :: BinOp Real Real)  <<@ UseAsDefault
	, de "subtract"		(dynamic BinOp (-)  :: BinOp Real Real)
	, de "multiply" 	(dynamic BinOp (*)  :: BinOp Real Real)
	, de "smaller"		(dynamic BinOp (<)  :: BinOp Real Bool)  <<@ UseAsDefault
	, de "greater"		(dynamic BinOp (>)  :: BinOp Real Bool)
	, de "equal real"	(dynamic BinOp (==) :: BinOp Real Bool)
	, de "equal bool"	(dynamic BinOp (==) :: BinOp Bool Bool)
	, de "and"			(dynamic BinOp (&&) :: BinOp Bool Bool)  <<@ UseAsDefault
	, de "or"			(dynamic BinOp (||) :: BinOp Bool Bool)
	]
  , DynamicConsGroup "Lists"
	[ lc "Ext bool" idExtBool <<@ HideIfOnlyChoice
	, lc "Ext real" idExtReal <<@ HideIfOnlyChoice
	]
  , DynamicConsGroup "Basic Editors"
	[ ce "Real value"	realLitEditor
	, ce "Bool value"	boolLitEditor
	]
  ]

idExtBool :: [Ext Bool] -> [Ext Bool]
idExtBool x = x

idExtReal :: [Ext Real] -> [Ext Real]
idExtReal x = x

:: Ext1 a b = None | Ext1 a b
:: BinOp a b = BinOp (a a->b)
:: Extension a :== Ext1 (BinOp a a) (Expr a)
:: BinOpExt1 a b :== (BinOp a b) (Expr a) (Expr a) (Extension b) -> Expr b

:: Ext b = Ext (BinOp b b) (Expr b)
:: BinOpExt a b :== (BinOp a b) (Expr a) (Expr a) [Ext b] -> Expr b

binOp :: (BinOp a b) (Expr a) (Expr a) [Ext b] -> Expr b
binOp (BinOp f) a b l = foldr (\(Ext (BinOp g) c) d.Oper g d c) (Oper f a b) l 

binOp1 :: (BinOp a b) (Expr a) (Expr a) -> Expr b
binOp1 (BinOp f) a b = Oper f a b

binOpExt1 :: (BinOp a b) (Expr a) (Expr a) (Extension b) -> Expr b
binOpExt1 (BinOp f) a b e = handleExt1 (Oper f a b) e

handleExt1 :: (Expr a) (Ext1 (BinOp a a) (Expr a)) -> Expr a
handleExt1 a (Ext1 (BinOp f) b) = Oper f a b
handleExt1 a None = a

de name dyn = functionConsDyn name name dyn
ce name edt = customEditorCons name name edt
lc name edt = listCons name name edt

gDefault{|Expr|}       _ = abort "No gDefault for Expr"
gEq{|Expr|}        _ _ _ = abort "No gEq for Expr"
JSONEncode{|Expr|} f b (Lit a) = f b a
JSONEncode{|Expr|} f b e = f b (eval e)
JSONDecode{|Expr|} f b l = (fmap Lit a,r) where (a,r) = f b l
//JSONEncode{|Expr|} _ _ _ = abort "No JSONEncode for Expr"
//JSONDecode{|Expr|} _ _ _ = abort "No JSONDecode for Expr"
gText{|Expr|}      _ _ _ = abort "No gText for Expr"
gEditor{|Expr|}  _ _ _ _ = abort "No gEditor for Expr"

intEditor :: Editor Int (?Int)
intEditor = gEditor{|*|} EditValue

realEditor :: Editor Real (?Real)
realEditor = gEditor{|*|} EditValue

realLitEditor :: Editor (Expr Real) (?(Expr Real))
realLitEditor = mapEditorRead (\(Lit r).r) (mapEditorWrite (fmap Lit) realEditor)

boolLitEditor :: Editor (Expr Bool) (?(Expr Bool))
boolLitEditor = mapEditorRead (\(Lit r).r) (mapEditorWrite (fmap Lit) boolEditor)

boolEditor :: Editor Bool (?Bool)
boolEditor = gEditor{|*|} EditValue

derive class iTask Num
numEditor :: Editor Num (?Num)
numEditor = gEditor{|*|} EditValue

basicClasses      = ["edit-task-base"]
horizontalClasses = ["edit-task-horizontal"]
verticalClasses   = ["edit-task-vertical"]
boxedClasses      = ["edit-task-boxed"]

applyHorizontalBoxedLayout = ApplyCssClasses (basicClasses ++ horizontalClasses ++ boxedClasses)
applyVerticalBoxedLayout = ApplyCssClasses (basicClasses ++ verticalClasses ++ boxedClasses)


