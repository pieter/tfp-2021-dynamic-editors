definition module ShipTypes

import iTasks

:: Ship =
	{ name :: String
	, size :: Int
	, kind :: Kind
	, flag :: Country
	, status :: Status
	, position :: Position
	}
:: Position =
	{ lat :: Real	// Latitude
	, lon :: Real	// Longitude
	}
:: Owner =
	{ name :: String
	, ship :: String
	, home :: Country
	}
:: Park =
	{ name :: String
	, country :: Country
	, position :: Position
	}
:: Port =
	{ name :: String
	, position :: Position
	}
:: Kind    = Navy | Fisher | Tanker | Cargo | Tugboat
:: Country = NL | UK | DE | FR | BE | BR | VE | PH | DK | LR | MT
:: Status  = Sailing | Moored

instance == Kind, Country, Ship, Park, Res, Position
instance < Res
instance toString Ship, Port, Park, Position, Kind, Country, Status

distance :: a b -> Real | toPosition a & toPosition b
class toPosition a :: a -> Position
instance toPosition Position
instance toPosition Ship
instance toPosition Park
instance toPosition Port
instance toPosition Res

derive class iTask Ship, Park, Kind, Position, Country, Status //, Res
derive gDefault Res
derive gEq Res
derive JSONEncode Res
derive JSONDecode Res
derive gText Res
derive gEditor Res

ships :: [Ship]
windParks :: [Park]
ports :: [Port]

:: Res = ShipResult Ship | ParkResult Park | BoolResult Bool | RealResult Real | StringResult String | PortResult Port

class toRes x :: x -> Res
instance toRes Ship, Bool, Real, Park, Port, String

matchName :: String String -> Bool

